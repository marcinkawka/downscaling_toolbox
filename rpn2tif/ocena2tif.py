#!/usr/bin/env python
import numpy as np 
import os
from rpn.rpn import RPN
from rpn.rpn_multi import MultiRPN   
from rpn_tools import *
from data_loaders import *
import seaborn as sns 
import matplotlib.pyplot as plt 
import datetime as dt
import argparse
import traceback

parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('scenario', metavar='scenario', type=str,help="nazwa scenariusza")
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
args = parser.parse_args()
scenario = args.scenario
start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))

RD = 287.05 #[J/KG/K]
rpn_dir ='/mnt/data1/ocena/2021/gvp018_v1/nodes/out_V04-3A-18CV1/'
output_folder = 'tif_output/ocena_daily/'

try:
	os.mkdir(output_folder)
except FileExistsError:
	pass

arr,geoTrans = load_raster('tif_output/daily/'+scenario+'/'+scenario+'_20210101.tif')

wsp_UTM = dict()
wsp_WGS = dict()

wsp_UTM['uLx']=geoTrans[0]
wsp_UTM['lLx']=geoTrans[0]

wsp_UTM['uLy']=geoTrans[3]
wsp_UTM['uRy']=geoTrans[3]

wsp_UTM['uRx']=arr.shape[1] * geoTrans[1] + geoTrans[0]
wsp_UTM['lRx']=arr.shape[1] * geoTrans[1] + geoTrans[0]

wsp_UTM['lRy']=arr.shape[0] * geoTrans[5] + geoTrans[3]
wsp_UTM['lLy']=arr.shape[0] * geoTrans[5] + geoTrans[3]

lat_min,lon_min = UTM2lonlat(wsp_UTM['lLx'],wsp_UTM['lLy'],'33N')
lat_max,lon_max = UTM2lonlat(wsp_UTM['uRx'],wsp_UTM['uRy'],'33N')

#tu jest źle, powinno być dzielenie przez dx w stopniach
dlon = (lon_max - lon_min) / abs(arr.shape[1])
dlat = (lat_max - lat_min) / abs(arr.shape[0])
delta = dt.timedelta(days=1)

while start_date <= end_date:
	date_str = start_date.strftime("%Y%m%d")
	
	date_str = start_date.strftime("%Y%m%d")
	path_list_km =[rpn_dir+date_str+'00/km'+date_str+'00-00-00_000000'+str(i).zfill(2)+'h' for i in range(1,25)]
	path_list_dm =[rpn_dir+date_str+'00/dm'+date_str+'00-00-00_000000'+str(i).zfill(2)+'h' for i in range(1,25)]
	
	r_km = MultiRPN(path_list_km)
	r_dm = MultiRPN(path_list_dm)

	pm10 = r_km.get_4d_field(varname='PM10')
	tt   = r_dm.get_4d_field(varname='TT')
	p0   = r_dm.get_4d_field(varname='P0')

	suma_dobowa = None
	licznik = 0
	#pętla po czasie dnia
	for key, value in pm10.items():
		src_array_pm10 = value[1.0]
		P0 = p0[key][0.0]
		TT = tt[key][1.0]

		rho_air = (P0*100)/(RD*(TT+273.15))
		src_array_pm10 = src_array_pm10 *  P0 * 100./(RD*(TT+273.15))  * 1.E+9 

		try:
			grid_x,grid_y,points = rpn2grid(path_list_km[0],varname='PM10',\
				lon_min=lon_min,lon_max=lon_max,lat_min=lat_min,lat_max=lat_max,dlat=dlat,dlon=dlon)
			array = np.flipud(rpn_interpolate(points,src_array_pm10.flatten(),grid_x,grid_y,method='linear'))
			if suma_dobowa is None:
				suma_dobowa = array
			else:
				suma_dobowa = suma_dobowa + array
			licznik = licznik + 1
		except:
			print("Nie udało się dla "+str(key))
			print(traceback.format_exc())
	
	r_dm.close()
	r_km.close()

	CreateGeoTiff(output_folder+scenario+'_'+date_str ,suma_dobowa/licznik,geoTrans)
	print(start_date)
	start_date += delta