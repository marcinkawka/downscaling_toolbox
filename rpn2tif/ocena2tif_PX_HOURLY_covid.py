#!/usr/bin/env python
import numpy as np 
import os
from rpn.rpn import RPN
from rpn.rpn_multi import MultiRPN   
from rpn_tools import *
from data_loaders import *
import seaborn as sns 
import matplotlib.pyplot as plt 
import datetime as dt
import argparse
import traceback

'''
Skrypt tworzy tify z wynikami dobowymi oceny na podstawie przykładowego tifa z Pasquilla
'''
parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('scenario', metavar='scenario', type=str,help="nazwa scenariusza")
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
args = parser.parse_args()
scenario = args.scenario
start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))

RD = 287.05 #[J/KG/K]
#rpn_dir ='/mnt/disk2/mkawka/ocena2021/px/'
rpn_dir = '/mnt/data1/struzw/remy/2020/cov_scen/out/'
#output_folder = 'tif_output/ocena_hourly_px/'+scenario+'/'
output_folder = '/mnt/fast01/mkawka/paszkwil/krakowskie_cov_scen/tif_gem/hourly/'

try:
	os.mkdir(output_folder)
except FileExistsError:
	pass

arr,geoTrans = load_raster('/mnt/fast01/mkawka/paszkwil/krakowskie_cov_scen/tif_output/daily/'+scenario+'_20200301.tif')
wsp_UTM = dict()
wsp_WGS = dict()

wsp_UTM['uLx']=geoTrans[0]
wsp_UTM['lLx']=geoTrans[0]

wsp_UTM['uLy']=geoTrans[3]
wsp_UTM['uRy']=geoTrans[3]

wsp_UTM['uRx']=arr.shape[1] * geoTrans[1] + geoTrans[0]
wsp_UTM['lRx']=arr.shape[1] * geoTrans[1] + geoTrans[0]

wsp_UTM['lRy']=arr.shape[0] * geoTrans[5] + geoTrans[3]
wsp_UTM['lLy']=arr.shape[0] * geoTrans[5] + geoTrans[3]

if scenario in ['Klodzko','Zgorzelecki','Karkonoski','Zabkowicki',\
	'Walbrzyski','Glubczycki','Nyski','Kamiennogorski']:
	UTM=33
else:
	UTM=34
print(UTM)

lat_min,lon_min = UTM2lonlat(wsp_UTM['lLx'],wsp_UTM['lLy'],str(UTM)+'N')
lat_max,lon_max = UTM2lonlat(wsp_UTM['uRx'],wsp_UTM['uRy'],str(UTM)+'N')

#tu jest źle, powinno być dzielenie przez dx w stopniach
dlon = (lon_max - lon_min) / abs(arr.shape[1])
dlat = (lat_max - lat_min) / abs(arr.shape[0])
delta = dt.timedelta(hours=1)


while start_date <= end_date:
	date_str = start_date.strftime("%Y%m%d")
	path_list_km =[rpn_dir+date_str+'00/km'+date_str+'00-00-00_000000'+str(i).zfill(2)+'h' for i in range(1,25)]
	path_list_dm =[rpn_dir+date_str+'00/dm'+date_str+'00-00-00_000000'+str(i).zfill(2)+'h' for i in range(1,25)]
	
	r_km = MultiRPN(path_list_km)
	r_dm = MultiRPN(path_list_dm)

	pt25 = r_km.get_4d_field(varname='PT25')
	ptco = r_km.get_4d_field(varname='PTCO')
	ph25 = r_km.get_4d_field(varname='PH25')
	phco = r_km.get_4d_field(varname='PHCO')
	tt   = r_dm.get_4d_field(varname='TT')
	p0   = r_dm.get_4d_field(varname='P0')
	suma_dobowa = None
	licznik = 0
	#pętla po czasie miesiaca

	for key, value in pt25.items():
		src_array_pm10 = 3*value[1.0]+2*ptco[key][1.0]+ph25[key][1.0]+phco[key][1.0]
		P0 = p0[key][0.0]
		TT = tt[key][1.0]

		rho_air = (P0*100)/(RD*(TT+273.15))
		src_array_pm10 = src_array_pm10 *  P0 * 100./(RD*(TT+273.15))  * 1.E+9 +3
		  
		try:
			grid_x,grid_y,points = rpn2grid(path_list_km[0],varname='PT25',\
				lon_min=lon_min,lon_max=lon_max,lat_min=lat_min,lat_max=lat_max,dlat=dlat,dlon=dlon)
			array = np.flipud(rpn_interpolate(points,src_array_pm10.flatten(),grid_x,grid_y,method='linear'))
			#points - punkty źródłowe z GEM
			date_str = start_date.strftime("%Y%m%d%H%M")
			CreateGeoTiff(output_folder+scenario+'_'+date_str ,array,geoTrans,UTM=UTM)
			print(start_date)
			
		except:
			print("Nie udało się dla "+str(key))
			print(traceback.format_exc())
		start_date += delta
  
	r_km.close()
	r_dm.close()

