import xarray as xr
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
'''
Skrypt do ekstrakcji z nc do csv dla porównania z obserwacjami do artykułu (na prośbę recenzenta MDPI)
'''

temp_folder = '/home/mkawka/wymiana/GEM_results/GEM_meteo/'
wind_folder = '/home/mkawka/wymiana/GEM_results/GEM_meteo/'
wind_prefix = 'wind_ts'
wind_sufix  = '.nc'
temp_prefix = 'pl25_'
temp_sufix  = '_surface_ts.nc'

def datetime_range(start, end, delta):
    current = start
    while current < end:
        yield current
        current += delta
        
temp_list=[]
wind_list=[]
time_list=[]
datestr='2021010100'
d0 = dt.datetime(2021,1,1,0,0)
plt.style.use('seaborn-white')

st_lat = 49.80692878602882
st_lon = 19.001509544952622
for i in range(365):
    data = d0+dt.timedelta(days=i)
    dts = [dt2 for dt2 in datetime_range(data, data+ dt.timedelta(days=1),dt.timedelta(hours=1))]
    print(datestr)
    try:
        datestr = data.strftime("%Y%m%d%H")
        ds = xr.open_dataset(temp_folder+temp_prefix+datestr+temp_sufix)
        datestr = data.strftime("%Y%m%d")
        ds_wind = xr.open_dataset(wind_folder+wind_prefix+datestr+wind_sufix)
        
        temp_list.extend(list(ds.sel(longitude=st_lon,latitude=st_lat,method='nearest').TT.values[:24].squeeze()))
        wind_list.extend(list(ds_wind.sel(longitude=st_lon,latitude=st_lat,method='nearest').VM.values[:24].squeeze()))
        time_list.extend(dts)
    except FileNotFoundError:
        print('Error!')    
df_model = pd.concat([pd.Series(temp_list,index=time_list),pd.Series(wind_list,index=time_list)],axis=1)
df_model.columns=['temp','wind']

'''
ładowanie obserwacji
'''
df_obs = pd.read_csv('csv_input/meteo2021.csv')
maska1 = (df_obs['station_name']=='Bielsko Biała')
maska2TT = (df_obs['paramname']=='TT')
maska2VM = (df_obs['paramname']=='VM')

df_temp = df_obs[maska1 & maska2TT].copy()
df_wind = df_obs[maska1 & maska2VM].copy()

df_temp.set_index(pd.to_datetime(df_temp['to_timestamp']),inplace=True)
df_wind.set_index(pd.to_datetime(df_wind['to_timestamp']),inplace=True)
del df_temp['station_name']
del df_temp['paramname']
del df_temp['to_timestamp']

del df_wind['station_name']
del df_wind['paramname']
del df_wind['to_timestamp']
'''
PLOTOWANIE
'''
SMALL_SIZE = 18
MEDIUM_SIZE = 18
BIGGER_SIZE = 18

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

fig = plt.figure(figsize=(20,12))
#fig = plt.figure(figsize=(12,10))
ax = fig.add_subplot()
xmajor_ticks = [dt2 for dt2 in datetime_range(d0, d0+ dt.timedelta(days=365),dt.timedelta(days=60))]
xminor_ticks = [dt2 for dt2 in datetime_range(d0, d0+ dt.timedelta(days=365),dt.timedelta(days=10))]
ymajor_ticks = np.arange(-25, 35, 10)
yminor_ticks = np.arange(-25, 35, 5)
ax = df_temp.plot(ax=ax)
ax = df_model['temp'].plot(ax=ax)
ax.set_xticks(xmajor_ticks)
ax.set_xticks(xminor_ticks, minor=True)
ax.set_yticks(ymajor_ticks)
ax.set_yticks(yminor_ticks, minor=True)

# And a corresponding grid
ax.grid(which='both')

# Or if you want different settings for the grids:
ax.grid(which='minor', alpha=0.2)
ax.grid(which='major', alpha=0.5)

plt.title('Temperature at Bielsko-Biała station')
plt.legend(['Observation','GEM-AQ'])
plt.xlabel('time')
plt.ylabel('temperature $[\degree C]$')
#plt.show()
plt.tight_layout()
plt.savefig('png/temp_validation.png', dpi=300)
plt.close()

fig = plt.figure(figsize=(20,12))
#fig = plt.figure(figsize=(12,10))
ax = fig.add_subplot()
xmajor_ticks = [dt2 for dt2 in datetime_range(d0, d0+ dt.timedelta(days=365),dt.timedelta(days=60))]
xminor_ticks = [dt2 for dt2 in datetime_range(d0, d0+ dt.timedelta(days=365),dt.timedelta(days=10))]
ymajor_ticks = np.arange(0, 15, 5)
yminor_ticks = np.arange(0, 15, 3)
ax = df_wind.plot(ax=ax)
ax = df_model['wind'].plot(ax=ax)
ax.set_xticks(xmajor_ticks)
ax.set_xticks(xminor_ticks, minor=True)
ax.set_yticks(ymajor_ticks)
ax.set_yticks(yminor_ticks, minor=True)

# And a corresponding grid
ax.grid(which='both')

# Or if you want different settings for the grids:
ax.grid(which='minor', alpha=0.2)
ax.grid(which='major', alpha=0.5)

plt.title('Wind speed at Bielsko-Biała station')
plt.legend(['Observation','GEM-AQ'])
plt.xlabel('time')
plt.ylabel('wind speed [m/s]')
#plt.show()
plt.tight_layout()
plt.savefig('png/vm_validation.png', dpi=300)
plt.close()