import numpy as np 
import os
from rpn.rpn import RPN   
from rpn_tools import *
from data_loaders import *
import seaborn as sns 
import matplotlib.pyplot as plt 
import datetime as dt
import argparse

parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
args = parser.parse_args()

start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))


input_folder ='/mnt/disk1/home/prognoza/pl25/diag_out/diag_conc_rpn/'
output_folder = 'tif_output/prognoza_daily/'

try:
	os.mkdir(output_folder)
except FileExistsError:
	pass

arr,geoTrans = load_raster('tif_output/daily/Klodzko_20210101.tif')

wsp_UTM = dict()
wsp_WGS = dict()

wsp_UTM['uLx']=geoTrans[0]
wsp_UTM['lLx']=geoTrans[0]

wsp_UTM['uLy']=geoTrans[3]
wsp_UTM['uRy']=geoTrans[3]

wsp_UTM['uRx']=arr.shape[1] * geoTrans[1] + geoTrans[0]
wsp_UTM['lRx']=arr.shape[1] * geoTrans[1] + geoTrans[0]

wsp_UTM['lRy']=arr.shape[0] * geoTrans[5] + geoTrans[3]
wsp_UTM['lLy']=arr.shape[0] * geoTrans[5] + geoTrans[3]


lat_min,lon_min = UTM2lonlat(wsp_UTM['lLx'],wsp_UTM['lLy'],'33N')
lat_max,lon_max = UTM2lonlat(wsp_UTM['uRx'],wsp_UTM['uRy'],'33N')
#tu jest źle, powinno być dzielenie przez dx w stopniach
dlon = (lon_max - lon_min) / abs(arr.shape[1])
dlat = (lat_max - lat_min) / abs(arr.shape[0])
delta = dt.timedelta(days=1)

while start_date <= end_date:
	date_str = start_date.strftime("%Y%m%d")
	input_file = 'pl25_'+date_str+'00_surface_lat_long_concentrations.rpn'
	r=RPN(input_folder+input_file)

	#średnia dobowa 2410
	src_field=r.get_2D_field_on_all_levels('2410')[1.0]
	r.close()

	grid_x,grid_y,points = rpn2grid(input_folder+input_file,'2410',lon_min,lon_max,lat_min,lat_max,dlat,dlon)
	north_array = rpn_interpolate(points,src_field.flatten(),grid_x,grid_y,method='nearest')

	CreateGeoTiff(output_folder+'Klodzko_'+date_str ,north_array,geoTrans)
	print(start_date)
	start_date += delta