#!/usr/bin/env python
import numpy as np 
import os
from utils.config import ScenarioConfig
from rpn.rpn import RPN
from rpn.rpn_multi import MultiRPN   
from rpn_tools import *
from utils.other_tools import UTM2lonlat
from utils.data_loaders import *
import seaborn as sns 
import matplotlib.pyplot as plt 
import datetime as dt
import argparse
import traceback


'''
Skrypt tworzy tify z wynikami godzinowymi oceny na podstawie przykładowego tifa z Pasquilla
'''
parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
config.load_ML_config()

RD = 287.05 #[J/KG/K]
rpn_dir = config.rpn_dir
rpn_file = rpn_dir+'control_diag_2019_B1_PX10.rpn_HOURLY'
output_folder = config.gem_tif_dir

try:
	os.mkdir(output_folder)
except FileExistsError:
	pass
config.start_date.strftime("%Y%m%d")
arr,geoTrans = load_raster(config.tif_daily_dir+config.scenario+'_'+config.start_date.strftime("%Y%m%d")+'.tif')

wsp_UTM = dict()
wsp_WGS = dict()

wsp_UTM['uLx']=geoTrans[0]
wsp_UTM['lLx']=geoTrans[0]

wsp_UTM['uLy']=geoTrans[3]
wsp_UTM['uRy']=geoTrans[3]

wsp_UTM['uRx']=arr.shape[1] * geoTrans[1] + geoTrans[0]
wsp_UTM['lRx']=arr.shape[1] * geoTrans[1] + geoTrans[0]

wsp_UTM['lRy']=arr.shape[0] * geoTrans[5] + geoTrans[3]
wsp_UTM['lLy']=arr.shape[0] * geoTrans[5] + geoTrans[3]



lat_min,lon_min = UTM2lonlat(wsp_UTM['lLx'],wsp_UTM['lLy'],str(config.UTM_zone)+'N')
lat_max,lon_max = UTM2lonlat(wsp_UTM['uRx'],wsp_UTM['uRy'],str(config.UTM_zone)+'N')

#tu jest źle, powinno być dzielenie przez dx w stopniach
dlon = (lon_max - lon_min) / abs(arr.shape[1])
dlat = (lat_max - lat_min) / abs(arr.shape[0])
delta = dt.timedelta(hours=1)


#points - punkty źródłowe z GEM
grid_x,grid_y,points = rpn2grid(rpn_file,varname='PX10',\
lon_min=lon_min,lon_max=lon_max,lat_min=lat_min,lat_max=lat_max,dlat=dlat,dlon=dlon)

r =RPN(rpn_file)
pm10 = r.get_4d_field('PX10')

suma_dobowa=None
licznik=0
start_date = config.subset_start_date
while  start_date <= config.subset_end_date:
    start_date += delta
    try:
        src_array_pm10 = pm10[start_date][1.0]
        array = (rpn_interpolate(points,src_array_pm10.flatten(),grid_x,grid_y,method='linear'))
        
        date_str = start_date.strftime("%Y%m%d%H%M")
        
        CreateGeoTiff(output_folder+config.scenario+'_'+date_str ,array,geoTrans,UTM=config.UTM_zone)
        licznik=0
        suma_dobowa=None               
        print(start_date)
        
        if start_date > config.subset_end_date:
            break

    except KeyError as e:
        print("Nie udało się dla "+str(start_date))
        
    

r.close()
