import numpy as np
from osgeo import gdal
from osgeo import osr
import matplotlib.pyplot as plt
import random


from data_loaders import *
import pprint as pp
from config import *
from config import config as cfg 

year = '2015'
nc_dir='/home/mkawka/wymiana/CAMS_TEMPO/'
nc_file = 'CAMS-REG-TEMPO_EUR_0.1x0.1_tmp_weights_v3.1_daily_'+year+'.nc'

gdal.PushErrorHandler('CPLQuietErrorHandler')
scenario = str(cfg['GENERAL']['scenario_name'])

nc_filename = nc_dir + nc_file

FH_A_pm10= load_temporal_profile(nc_filename,'FH_A_pm10',24)
FH_saturday_F= load_temporal_profile(nc_filename,'FH_saturday_F',24)
FH_sunday_F= load_temporal_profile(nc_filename,'FH_sunday_F',24)
FH_weekday_F= load_temporal_profile(nc_filename,'FH_weekday_F',24)

coords_PL=dict()
coords_PL['ul_lat']=54.75
coords_PL['ul_lon']=14.25
coords_PL['lr_lon']=24.25
coords_PL['lr_lat']=49.0

FH_A_pm10_PL= load_temporal_profile(nc_filename,'FH_A_pm10',24,coordinates=coords_PL)
FH_saturday_F_PL= load_temporal_profile(nc_filename,'FH_saturday_F',24,coordinates=coords_PL)
FH_sunday_F_PL= load_temporal_profile(nc_filename,'FH_sunday_F',24,coordinates=coords_PL)
FH_weekday_F_PL= load_temporal_profile(nc_filename,'FH_weekday_F',24,coordinates=coords_PL)

plt.style.use('ggplot')

fig, axes = plt.subplots(nrows=2, ncols=2,figsize=(20,20))
axes[0,0].plot(FH_A_pm10)
axes[0,0].plot(FH_A_pm10_PL)
axes[0,0].legend([scenario,'avg PL'])
axes[0,0].grid(True)
axes[0,0].set_title('FH_A_pm10 (kombyt) '+year+'')

axes[0,1].plot(FH_sunday_F)
axes[0,1].plot(FH_sunday_F_PL)
axes[0,1].legend([scenario,'avg PL'])
axes[0,1].grid(True)
axes[0,1].set_title('traffic - sunday '+year+'')


axes[1,1].plot(FH_saturday_F)
axes[1,1].plot(FH_saturday_F_PL)
axes[1,1].legend([scenario,'avg PL'])
axes[1,1].grid(True)
axes[1,1].set_title('traffic - saturday '+year)


axes[1,0].plot(FH_weekday_F)
axes[1,0].plot(FH_weekday_F_PL)
axes[1,0].legend([scenario,'avg PL'])
axes[1,0].grid(True)
axes[1,0].set_title('traffic - weekday '+year)

fig.suptitle('Profile godzinowe')
plt.savefig('png/temporal_profiles/camsTEMPO'+year+'_v31.png')
plt.close()