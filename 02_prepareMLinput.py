#!/usr/bin/env python
import pandas as pd 
import matplotlib.pyplot as plt
import datetime as dt
from utils.config import ScenarioConfig
import argparse
'''
Skrypt do przygotowania wejścia do modelu ML
'''

parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()


df_gauss = pd.read_csv(config.csv_output_dir+config.scenario+'_'+\
    config.start_date.strftime("%Y%m%d")+'_'+config.end_date.strftime("%Y%m%d")+'.csv')

#GIOŚ observations
df_obs = pd.read_csv(config.observations_file)
df_obs.set_index(pd.to_datetime(df_obs["to_timestamp"]),inplace=True)
del df_obs["to_timestamp"]
maska_obs = (df_obs['station_code'].isin(config.pm10_stations))

#zamiana kolumno-wartości na kolumny
df_obs = pd.pivot_table(df_obs[maska_obs],\
    values='pm10',\
    columns='station_code',\
    index=df_obs[maska_obs].index)

#GEM-AQ results
#sc='base'
#dotychczasowe
#csv_gem = 'csv_input/px_ocena_ts'+config.start_date.strftime('%Y%m%d')+'.csv'
#CoCoMobility
#csv_gem = 'csv_input/px_ocena_tsWAW'+str(args.d0)+'_'+sc+'.csv'
#csv_gem = 'csv_input/px_ocena_ts'+str(args.d0)+'_'+sc+'.csv'

df_gem = pd.read_csv(config.gem_file)
df_gem.set_index(pd.to_datetime(df_gem["Unnamed: 0"]),inplace=True)
del df_gem["Unnamed: 0"]

df_gauss.set_index(pd.to_datetime(df_gauss["Unnamed: 0"]),inplace=True)
del df_gauss["Unnamed: 0"]

#joining together
res = pd.DataFrame(df_gem[config.pm10_stations])
res = res.join(df_gauss,rsuffix='_gauss',lsuffix='_gem')

df_obs.columns = df_obs.columns.map(lambda x: str(x) + '_measurement')
res= res.join(df_obs)
# We don't delete since this removes the whole row
#res.dropna(inplace=True)
print("The following data will be used for ML training:")
print(res.describe().T)

res.to_csv(config.csv_output_dir+config.scenario+'_'+\
    config.start_date.strftime("%Y%m%d")+'_'+\
    config.end_date.strftime("%Y%m%d")+'_MLready.csv',float_format='%.3f')