
import xarray as xr
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import pandas as pd
import numpy as np
import locale
import matplotlib.dates as dates
'''
Skrypt do plotowania serii czasowych z wyników PM10 na prośbę recenzenta z MDPI
'''
csv_inputML = 'Poludniowoslaskie_20210101_20211231_MLready.csv'
csv_dir='/mnt/fast01/mkawka/paszkwil/poludniowoslaskie2021/csv_output/'

stacje = ['SlGoczaUzdroMOB','SlRybniBorki','SlUstronSana','SlWodzGalczy',\
    'SlZywieKoper','SlCiesChopin','SlBielKossak','MpSuchaNiesz','MpOswiecBema']
df =pd.read_csv(csv_dir+csv_inputML)
df.set_index(pd.to_datetime(df['Unnamed: 0']),inplace=True)

def datetime_range(start, end, delta):
    current = start
    while current < end:
        yield current
        current += delta
'''
PLOTOWANIE
'''
SMALL_SIZE = 18
MEDIUM_SIZE = 18
BIGGER_SIZE = 18
d0 = dt.datetime(2021,1,1,0,0)
plt.style.use('seaborn-white')
locale.setlocale(locale.LC_TIME,'en_US')
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
xmajor_ticks = [dt2 for dt2 in datetime_range(d0, d0+ dt.timedelta(days=365),dt.timedelta(days=90))]
xminor_ticks = [dt2 for dt2 in datetime_range(d0, d0+ dt.timedelta(days=365),dt.timedelta(days=20))]
ymajor_ticks = np.arange(0, 350, 50)
yminor_ticks = np.arange(0, 350, 10)

for st in stacje:
    fig = plt.figure(figsize=(20,10))
    ax = fig.add_subplot()
    df1 = df[[st+'_measurement',st+'_gem',st+'_gauss']]
    dfmt = dates.DateFormatter('%b')
    locale.setlocale(locale.LC_TIME,'en_US')
    ax.xaxis.set_major_formatter(dfmt)
    
    ax = df1.plot(ax=ax)    
    ax.set_ylim([-10,300])
    #ax.set_xticks(xmajor_ticks)
    ax.set_xticks(xminor_ticks, minor=True)
    ax.set_yticks(ymajor_ticks)
    ax.set_yticks(yminor_ticks, minor=True)
    #ax.xaxis.set_major_locator(ticker.MultipleLocator(5*24*60))
    
    # And a corresponding grid
    ax.grid(which='both')

    # Or if you want different settings for the grids:
    ax.grid(which='minor', alpha=0.2)
    ax.grid(which='major', alpha=0.5)
        
    # rotates labels 
    #plt.setp( ax.xaxis.get_majorticklabels(), rotation=-45 ) 
    
    plt.title('$PM_{10}$ concentration at '+st+' station')
    plt.legend(['Observation','GEM-AQ','Gaussian model'])
    plt.xlabel('time')
    plt.ylabel('$PM_{10}$ concentration [$\mu g /m^3$]')
    plt.tight_layout()
    plt.savefig('png/'+st+'_validation.png', dpi=300)
    plt.close()