import pandas as pd
import matplotlib
import matplotlib.pyplot as plt


font = {'family' : 'normal',\
    'weight' : 'normal',\
    'size'   : 16}
matplotlib.rc('font', **font)


df=pd.read_csv('csv_input/meteo2021.csv')
df['date']=pd.to_datetime(df['to_timestamp'])
df['month']=df['date'].dt.month
df_bb=df[df['station_name']=='Bielsko Biała'].copy()

df_bbtt = df_bb[df_bb['paramname']=='TT'][['value','month']].copy()
df_bbvm = df_bb[df_bb['paramname']=='VM'][['value','month']].copy()

boxprops = dict(linestyle='-', linewidth=2, color='k')
whiskerprops = dict(linestyle='-', linewidth=2, color='k')
capprops = dict(linestyle='-', linewidth=3, color='k')

#fig = plt.figure(figsize=(10, 10))
#ax = fig.subfigures(1, 1)
fig, ax = plt.subplots(figsize=(12,10))
axes=df_bbtt[['value','month']].boxplot(by='month',ax=ax,boxprops=boxprops, whiskerprops=whiskerprops, capprops=capprops)

fig = axes.get_figure()
fig.suptitle('')
plt.ylabel('temperature $[\degree C]$')
plt.xlabel('month')
plt.title('Temperature at Bielsko-Biała station in 2021')

plt.savefig('png/temp.png',dpi=300)
plt.close()

fig, ax = plt.subplots(figsize=(12,10))
axes=df_bbvm[['value','month']].boxplot(by='month',ax=ax,boxprops=boxprops, whiskerprops=whiskerprops, capprops=capprops)

fig = axes.get_figure()
fig.suptitle('')
plt.ylabel('wind speed $[m/s]$')
plt.xlabel('month')
plt.title('Wind speed at Bielsko-Biała station in 2021')

plt.savefig('png/vm.png',dpi=300)
plt.close()