#!/bin/bash
startDate=$1
endDate=$2
scenario=$3
if [ -z "$scenario" ]
then 
echo '_______________________________'
echo 'Proszę wybrać scenariusz sposród dostępnych:'
for dost in `ls tif_output/daily/`
do
    echo $dost
done
exit
fi

echo
echo '_______________________________'
#./extractPaszkwilTimeseries.py $scenario $startDate $endDate

#Skrypt do wyciągania serii czasowej w danym punkcie z katalogu z godzinowymi tiffami
#Wersja z więcej niż 1 stacją na obszarze obliczeń
#./extractPaszkwilTimeseries_multiple.py $scenario $startDate $endDate

#analiza korelacji paszkwilla vs. gios i gem
#./analyze_results.py $scenario $startDate
#./analyze_results_multiple.py $scenario $startDate $endDate

#./prepareMLinput.py $scenario $startDate
#./prepareMLinput_multiple.py $scenario $startDate $endDate

# trenowanie na wartościach godzinowych
#./train_ML.py $scenario $startDate
#./train_ML_multiple.py $scenario $startDate $endDate

# dobowa ekstrakcja oceny
#./ocena2tif.py  $scenario $startDate $endDate

#apply na wartościach dobowych
#./applyML.py  $scenario $startDate $endDate
#./applyML_hourly.py  $scenario $startDate $endDate

./extractResultsTS_HOURLY.py $scenario $startDate $endDate
./analyze_RF_TS_multiple.py $scenario $startDate $endDate
#./calculate_agregates.py $scenario $startDate $endDate
