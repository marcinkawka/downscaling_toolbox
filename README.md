# downscaling_toolbox

A simplified model for obtaining high resolution results from regional air-quality model.

Detailed documentation can be found here:
https://wiki.zmaik.pl/index.php/Random_Forest_Downscalling

How to run:
`git clone https://gitlab.com/marcinkawka/downscaling_toolbox.git`
