from data_loaders import *
import matplotlib.pyplot as plt
import datetime as dt
import argparse
from config import ScenarioConfig
import pydot
import numpy as np
import os 

'''
Skrypt do generowania danych wejściowych dla Sherpy w csv
'''
parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
config.load_ML_config()

start_date = config.subset_start_date
end_date = config.subset_end_date
csv_dir = config.csv_output_dir+'sherpa/'

os.makedirs(csv_dir,exist_ok=True)

df_rf = pd.read_csv(config.csv_output_dir+'RF_'+config.scenario+'_'+config.train_stations[0]+'_'+start_date.strftime('%Y%m%d')+'_'+end_date.strftime('%Y%m%d')+'.csv')
df_rf.set_index(['Unnamed: 0'],inplace=True)
stations =df_rf.columns

df_gios = pd.read_csv('csv_input/pomiaryPM10_'+str(config.start_date.year)+'.csv')
df_gios = df_gios.drop_duplicates(subset=['to_timestamp','station_code']).pivot(columns=['station_code'],index=['to_timestamp'])
df_rf.set_index(pd.to_datetime(df_rf.index),inplace=True)
df_gios.set_index(pd.to_datetime(df_gios.index),inplace=True)
df_gios.columns = ["".join(x[1]) for x in df_gios.columns]

rf_dir = csv_dir +'RF/'
gauss_dir = csv_dir +'GAUSS/'
gem_dir = csv_dir +'GEM/'
gios_dir = csv_dir +'GIOS/'

os.makedirs(rf_dir,exist_ok=True)
#os.makedirs(gauss_dir,exist_ok=True)
#os.makedirs(gem_dir,exist_ok=True)
os.makedirs(gios_dir,exist_ok=True)

for st in stations:
    df1 = pd.DataFrame(df_rf[st].copy())
    df1['year']	=df1.index.year
    df1['month']=df1.index.month
    df1['day']	=df1.index.day
    df1['hour']	=df1.index.hour
    df1.rename(columns={st:'PM10'})
    df1.to_csv(rf_dir+str(st)+".csv")
    
for st in stations:
    try:
        df1 = pd.DataFrame(df_gios[st].copy())
        df1['year']	=df1.index.year
        df1['month']=df1.index.month
        df1['day']	=df1.index.day
        df1['hour']	=df1.index.hour
        df1.rename(columns={st:'PM10'})
        df1.to_csv(gios_dir+str(st)+".csv")
    except KeyError:
        print('Stacja '+st+" nie znaleziona")