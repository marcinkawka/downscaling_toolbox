#!/usr/bin/env python3
from utils.config import ScenarioConfig
import argparse
import pandas as pd
from utils.calculations import analyze_corr
from dateutil.relativedelta import relativedelta
import os
import datetime as dt
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt
import seaborn as sns
'''
Jedyny słuszny skrypt do analizy wyników gaussa i gema względem obserwacji
'''
parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
#docelowo wczytywane z lini poleceń
analyze_RF = False

pd.options.display.float_format = '{:,.2f}'.format
plt.style.use('ggplot')

#Gaussian results
df_gauss = pd.read_csv(config.csv_output_dir+config.scenario+'_'+\
    config.start_date.strftime("%Y%m%d")+'_'+config.end_date.strftime("%Y%m%d")+'.csv')

print('Podsumowanie gaussa')
print(df_gauss.describe().T)

#GIOŚ observations
df_obs = pd.read_csv(config.observations_file)
df_obs.set_index(pd.to_datetime(df_obs["to_timestamp"]),inplace=True)
del df_obs["to_timestamp"]
maska_obs = (df_obs['station_code'].isin(config.pm10_stations))

#zamiana kolumno-wartości na kolumny
df_obs = pd.pivot_table(df_obs[maska_obs],\
    values='pm10',\
    columns='station_code',\
    index=df_obs[maska_obs].index)

print('Podsumowanie obs')
print(df_obs.describe().T)

#GEM-AQ results
df_gem = pd.read_csv('csv_input/px_ocena_ts'+config.start_date.strftime('%Y%m%d')+'.csv')
df_gem.set_index(pd.to_datetime(df_gem["Unnamed: 0"]),inplace=True)
del df_gem["Unnamed: 0"]

print('Podsumowanie GEM-AQ')
print(df_gem[config.pm10_stations].describe().T)

df_gauss.set_index(pd.to_datetime(df_gauss["Unnamed: 0"]),inplace=True)
del df_gauss["Unnamed: 0"]

if analyze_RF:
    res = res.join(df_wszystko)
    to_be_analyzed = ['gem','gauss','RF']
else:
    to_be_analyzed = ['gem','gauss']
'''
#optional RF analysis

if analyze_RF:
    df_wszystko = pd.read_csv('csv_output/'+scenario+'_'+start_date.strftime('%Y%m%d')+'_'+end_date.strftime('%Y%m%d')+sufixRF+'_caloscHourly.csv')
    df_wszystko.set_index(pd.to_datetime(df_wszystko["Unnamed: 0"]),inplace=True)
    lista = []
    for col in df_wszystko.columns:
        if col[-2:]=='RF':
            lista.append(col)

    df_wszystko = df_wszystko[lista]
'''
#joining together
res = pd.DataFrame(df_gem[config.pm10_stations])
res = res.join(df_gauss,rsuffix='_gauss',lsuffix='_gem')

df_obs.columns = df_obs.columns.map(lambda x: str(x) + '_measurement')
res= res.join(df_obs)

res_avg24= res.groupby(pd.Grouper(freq="1D")).mean()
res_max24= res.groupby(pd.Grouper(freq="1D")).max()


#korelacje gauss vs. measurements
coors1h_all,mse = analyze_corr(  res,config.pm10_stations)
coors24h_avg,mse = analyze_corr(res_avg24,config.pm10_stations)
coors24h_max,mse = analyze_corr(res_max24,config.pm10_stations)

coors_monthly_1h = dict()
coors_monthly_avg = dict()
coors_monthly_max = dict()

mse_monthly_1h = dict()
mse_monthly_avg = dict()
mse_monthly_max = dict()

delta = relativedelta(months=+1)

os.makedirs(config.png_diags_dir, exist_ok=True)


print('\t\tKorelacje dla wartości godzinowych:')
print('________pogrupowane____GODZINOWO_________')
[print(key,':',value) for key, value in coors1h_all.items()]
print('________pogrupowane____DOBOWO(średnie)________')
[print(key,':',value) for key, value in coors24h_avg.items()]
print('________pogrupowane____DOBOWO(max)________')
[print(key,':',value) for key, value in coors24h_max.items()]


start_year = config.start_date.year
mon0 = config.start_date.month
monk = config.end_date.month

for sufix in to_be_analyzed:
    for miesiac in range(mon0,monk+1):
        res_tmp = res[dt.datetime(start_year,miesiac,1):dt.datetime(start_year,miesiac,1)+delta]
        coors_monthly_1h[miesiac],mse_monthly_1h[miesiac] = analyze_corr(res_tmp,config.pm10_stations,sufix)

        res_tmp = res_avg24[dt.datetime(start_year,miesiac,1):dt.datetime(start_year,miesiac,1)+delta]
        coors_monthly_avg[miesiac],mse_monthly_avg[miesiac] = analyze_corr(res_tmp,config.pm10_stations,sufix)

        res_tmp = res_max24[dt.datetime(start_year,miesiac,1):dt.datetime(start_year,miesiac,1)+delta]
        coors_monthly_max[miesiac],mse_monthly_max[miesiac] = analyze_corr(res_tmp,config.pm10_stations,sufix)
    
    df_monthly_coors_1h = pd.DataFrame(coors_monthly_1h)
    df_monthly_mse_1h = pd.DataFrame(mse_monthly_1h)

    df_monthly_coors_avg = pd.DataFrame(coors_monthly_avg)
    df_monthly_mse_avg = pd.DataFrame(mse_monthly_avg)

    df_monthly_coors_max = pd.DataFrame(coors_monthly_max)
    df_monthly_mse_max = pd.DataFrame(mse_monthly_max)
    if sufix=='RF':
        sufix = sufix+sufixRF

    #1h
    fig, axes = plt.subplots(nrows=1, ncols=1,figsize=(14,10))
    data_with_minus = df_monthly_coors_1h.applymap(lambda x: str(x).replace('-', '−'))
    ax = sns.heatmap(df_monthly_coors_1h,annot=data_with_minus, fmt="",cmap="YlGnBu",ax=axes,vmax=0.6)
    ax.set_xlabel('month')
    ax.set_title('Correlation '+sufix+' vs measurement '+str(config.start_date.year))
    plt.savefig(config.png_diags_dir+'coors_1h_'+sufix+'.png',bbox_inches='tight', dpi=300)
    plt.close()
    
    fig, axes = plt.subplots(nrows=1, ncols=1,figsize=(14,10))
    data_with_minus = df_monthly_mse_1h.applymap(lambda x: str(x).replace('-', '−'))
    ax = sns.heatmap(df_monthly_mse_1h,annot=data_with_minus, fmt="",cmap="YlGnBu",ax=axes,norm=LogNorm())
    ax.set_xlabel('month')
    ax.set_title('RMSE '+sufix+' vs measurement '+str(config.start_date.year))
    plt.savefig(config.png_diags_dir+'mse_1h_'+sufix+'.png',bbox_inches='tight', dpi=300)
    plt.close()

    #AVG
    fig, axes = plt.subplots(nrows=1, ncols=1,figsize=(14,10))
    data_with_minus = df_monthly_coors_avg.applymap(lambda x: str(x).replace('-', '−'))
    ax = sns.heatmap(df_monthly_coors_avg,annot=data_with_minus, fmt="",cmap="YlGnBu",ax=axes,vmax=0.6)
    ax.set_xlabel('month')
    ax.set_title('Correlation avg24 '+sufix+' vs measurement '+str(config.start_date.year))
    plt.savefig(config.png_diags_dir+'/coors_avg24_'+sufix+'.png',bbox_inches='tight', dpi=300)
    plt.close()

    fig, axes = plt.subplots(nrows=1, ncols=1,figsize=(14,10))
    data_with_minus = df_monthly_mse_avg.applymap(lambda x: str(x).replace('-', '−'))
    ax = sns.heatmap(df_monthly_mse_avg,annot=data_with_minus, fmt="",cmap="YlGnBu",ax=axes)
    ax.set_xlabel('month')
    ax.set_title('RMSE avg24 '+sufix+' vs measurement '+str(config.start_date.year))
    plt.savefig(config.png_diags_dir+'mse_avg24_'+sufix+'.png',bbox_inches='tight', dpi=300)
    plt.close()

    #MAX
    fig, axes = plt.subplots(nrows=1, ncols=1,figsize=(14,10))
    data_with_minus = df_monthly_coors_max.applymap(lambda x: str(x).replace('-', '−'))
    ax = sns.heatmap(df_monthly_coors_max,annot=data_with_minus, fmt="",cmap="YlGnBu",ax=axes,vmax=0.6)
    ax.set_xlabel('month')
    ax.set_title('Correlation max24 '+sufix+' vs measurement '+str(config.start_date.year))
    plt.savefig(config.png_diags_dir+'coors_max24_'+sufix+'.png',bbox_inches='tight', dpi=300)
    plt.close()

    fig, axes = plt.subplots(nrows=1, ncols=1,figsize=(14,10))
    data_with_minus = df_monthly_mse_max.applymap(lambda x: str(x).replace('-', '−'))
    ax = sns.heatmap(df_monthly_mse_max,annot=data_with_minus, fmt="",cmap="YlGnBu",ax=axes)
    ax.set_xlabel('month')
    ax.set_title('MSE max24 '+sufix+' vs measurement '+str(config.start_date.year))
    plt.savefig(config.png_diags_dir+'mse_max24_'+sufix+'.png',bbox_inches='tight', dpi=300)
    plt.close()
    
'''
   
mnoznik = 1e0
for st in config.pm10_stations:
    try:
        res_1st = res[[st+'_gem',st+'_measurement',st+'_gauss']]
        res_1st_avg = res_avg24[[st+'_gem',st+'_measurement',st+'_gauss']]
        res_1st_max = res_max24[[st+'_gem',st+'_measurement',st+'_gauss']]
        
        fig, axes = plt.subplots(nrows=3, ncols=1,figsize=(20,30))
        res_1st[st+'_gem'].plot(ax=axes[0],lw=0.5)
        res_1st[st+'_measurement'].plot(ax=axes[0],lw=0.5)
        (res_1st[st+'_gauss']*mnoznik).plot(ax=axes[0],lw=0.5)
        axes[0].set_title(st+' hourly ts')
        axes[0].set_xlim([config.start_date,config.end_date])
        
        res_1st_avg[st+'_gem'].plot(ax=axes[1],lw=1)
        res_1st_avg[st+'_measurement'].plot(ax=axes[1],lw=1)
        (res_1st_avg[st+'_gauss']*mnoznik).plot(ax=axes[1],lw=1)
        axes[1].set_title(st+' daily avg ts')
        axes[1].set_xlim([config.start_date,config.end_date])
        
        res_1st_max[st+'_gem'].plot(ax=axes[2],lw=0.5)
        res_1st_max[st+'_measurement'].plot(ax=axes[2],lw=1)
        (res_1st_max[st+'_gauss']*mnoznik).plot(ax=axes[2],lw=1)
        axes[2].set_title(st+' daily max ts')
        axes[2].set_xlim([config.start_date,config.end_date])
        
        plt.legend([st+'_gem-aq',st+'_measurement',st+'_gauss'])
        plt.savefig(config.png_diags_dir+'TS_'+st+'.png',dpi=150)
        plt.close()
        print('Wykres dla '+st+' został zapisane w '+config.png_diags_dir+'/TS_KodStacji')
    except KeyError:
        print(st+' not found!')
'''