#!/usr/bin/env python
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

import sys
import math 
import time
from scipy import ndimage, misc
import calendar
import os

import pandas as pd
import cProfile

from utils.config import ScenarioConfig
from utils.data_loaders import *
from utils.iterative_calculations import iterate_sources
from utils.other_tools import lonlat2UTM
from utils.calculations import get_stability_class

import pprint as pp
import ctypes

import traceback
import datetime as dt
import argparse
import traceback

parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

def main():
	config = ScenarioConfig(args.scenario_file)
	config.load_config()
	config.create_dirs()

	ZR_kombyt,ZR_industrial,ZR_traffic = load_emissions(config)
	C, wsp, geoTrans = create_arrays(config)
	#monthly avereged concentration
	C_monthly = np.zeros_like(C,dtype=np.float32)

	licznik_dni=0
	delta = dt.timedelta(days=1)
	current_date = config.start_date
	old_month = current_date.month
	while current_date <= config.end_date:
		date = current_date.strftime("%Y%m%d")
		month = current_date.month
		year = current_date.year
  
		print("\n"+date)
		try:
			#gradient Richardson number
			Ri_ts = load_Ri(date,config)

			#stability timeseries
			stab_ts = [] 
			m_ts = []
			for Ri in Ri_ts:
				cl,m = get_stability_class(Ri)
				stab_ts.append(cl)
				m_ts.append(m)

			print(stab_ts)

			#loading vertical profile
			Z,T = load_ambientTemp(date,config)

   			#loading temporal profile
			prof_traffic,prof_kombyt, prof_industrial = load_emission_profiles(current_date)
   
			#daily avereged concentration
			C_daily = np.zeros_like(C,dtype=np.float32)
		
			for hour in range(1,25):
				print('_'+str(hour).zfill(2),end="")
				(M_10, theta) = load_winds_VM(date,hour,config)
			
				#Rotation matrix 
				config.R=np.array([[np.cos(3*(np.pi/2)+theta),-np.sin(3*(np.pi/2)+theta)],\
					[np.sin(3*(np.pi/2)+theta),np.cos(3*(np.pi/2)+theta)]])

				config.R_inv=np.array([[np.cos(3*(np.pi/2)+theta),np.sin(3*(np.pi/2)+theta)],\
					[-np.sin(3*(np.pi/2)+theta),np.cos(3*(np.pi/2)+theta)]])

				config.wsp_R = (config.R @ wsp.T).T				
				start_time = time.time()			
				#empty concentration matrix for the current timestep
				config.C_tmp = np.zeros(wsp.shape[0],dtype=np.float64)

				iterate_sources(ZR_kombyt,M_10,stab_ts[hour-1],T[28][hour-1],m_ts[hour-1],'domest',config,\
        			config.emissions_reduction_factor_kombyt*prof_kombyt[hour-1])
    
				iterate_sources(ZR_industrial,M_10,stab_ts[hour-1],T[28][hour-1],m_ts[hour-1],'industrial',config,\
				config.emissions_reduction_factor_industrial*prof_industrial[hour-1] )
				
				iterate_sources(ZR_traffic,M_10,stab_ts[hour-1],T[28][hour-1],m_ts[hour-1],'traffic',config,\
				config.emissions_reduction_factor_traffic*prof_traffic[hour-1])

				#wracamy do pierwotnych wymiarów
				C_step = config.C_tmp.reshape(C_daily.shape,order='A')*1e6
				
				#if cfg.getboolean('OUTPUT','results_smoothing'):
				#	C_step=ndimage.uniform_filter(C_step, size=5, mode='constant')
				
				#agregataion
				C_daily += C_step

				print("--- {:3.3f}".format(time.time()-start_time))
				#fig, ax = plt.subplots(figsize=(15,10)) 
				
				#saving output
				if config.png_hourly_output:
					sns_plot = sns.heatmap(C,cmap='jet',cbar=True,vmin=0,vmax=10)
					fig = sns_plot.get_figure()
					fig.savefig("png_output/output_"+date+'_'+str(hour)+".png")
					plt.close()

				if config.tif_hourly_output:
					CreateGeoTiff(config.tif_hourly_dir+'/'+config.scenario+"_"+date+'_'+str(hour).zfill(2),\
         			C_step,geoTrans,UTM=config.UTM_zone)
				
				sys.stdout.flush()
			
			C_daily = C_daily/24
			C_monthly += C_daily
   
			licznik_dni += 1
			old_month= month
			days = calendar.monthrange(current_date.year,current_date.month)[1]
			current_date += delta
			if current_date.month != old_month:
				C_monthly = C_monthly /days
				if config.tif_monthly_output:
					CreateGeoTiff(config.tif_monthly_dir+'/'+config.scenario+"_"+date, C_monthly,geoTrans,UTM=config.UTM_zone)
     
				C_monthly = np.zeros_like(C_monthly)
     
			#saving daliy data
			CreateGeoTiff(config.tif_daily_dir+'/'+config.scenario+"_"+date, C_daily,geoTrans)
   
		except Exception as e:
			pp.pprint(traceback.format_exc())

	if config.tif_monthly_output:
		CreateGeoTiff(config.tif_monthly_dir+'/'+config.scenario+"_"+date, C_monthly,geoTrans,UTM=config.UTM_zone)


if __name__ == "__main__":
	main()
		
