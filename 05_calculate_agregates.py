#!/usr/bin/env python


import matplotlib.pyplot as plt
import datetime as dt
import argparse
from utils.config import ScenarioConfig
from utils.data_loaders import *
import pydot
import numpy as np
import os 



parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
config.load_ML_config()

rf_folder = config.tif_hourly_ML_dir
rf_folder_daily = config.tif_daily_ML_dir
prognoza_folder = config.gem_tif_dir+'hourly/'
gauss_folder  = config.tif_hourly_dir
print(prognoza_folder)
#nulujemy ścieżkę jeśli ma się policzyć agregat tylko z jednych danych
#rf_folder = ''
#prognoza_folder = ''
#gauss_folder  = ""


output_folder = config.tif_final_dir 
sufix=''
if config.include_dow:
	sufix += '_dow'
if config.include_meteo_obs:
	sufix += '_meteo_obs'     
	
os.makedirs(output_folder,exist_ok=True)

res_gem = []
res_rf = []
res_gauss =[]

daily_gem = []
daily_gauss = []
daily_rf = []
daily_rf2 = []

if os.path.isdir(rf_folder):
	start_date = config.subset_start_date
	end_date = config.subset_end_date
	delta = dt.timedelta(hours=1)
	hour = 1
	while start_date <= end_date:
		date_str = start_date.strftime("%Y%m%d%H%M")
		try:
			arr_rf,geoTrans = load_raster(rf_folder +config.scenario+'_'+date_str+'.tif')
			res_rf.append(arr_rf)
		except AttributeError:
			print('Failed to RFopen '+rf_folder +config.scenario+'_'+date_str+'.tif')

		start_date += delta
		hour+=1
		if hour==25:
			hour=1
			daily_rf.append( np.nanmean(np.array(res_rf[-24:]),axis=0))
	all_res_rf = np.array(res_rf)
	all_res_rf_daily = np.array(daily_rf)

if os.path.isdir(rf_folder_daily):
	start_date = config.subset_start_date
	end_date = config.subset_end_date
	delta = dt.timedelta(days=1)
	day = 1
	while start_date <= end_date:
		date_str = start_date.strftime("%Y%m%d")
		try:
			arr_rf,geoTrans = load_raster(rf_folder_daily +config.scenario+'_'+date_str+'.tif')
			daily_rf2.append(arr_rf)
		except AttributeError:
			print('Failed to open '+rf_folder_daily +config.scenario+'_'+date_str+'.tif')
		start_date += delta
		day+=1
  
	all_res_rf_daily2 = np.array(daily_rf2)

   
if os.path.isdir(prognoza_folder):
	start_date = config.subset_start_date
	end_date = config.subset_end_date
	delta = dt.timedelta(hours=1)
	hour = 1
	while start_date <= end_date:
		try:
			date_str = start_date.strftime("%Y%m%d%H%M")
			arr_gem,geoTrans = load_raster(prognoza_folder+config.scenario+'_'+date_str+'.tif')
			res_gem.append(arr_gem)

		except AttributeError:
			print('Failed to PROGNOZAopen '+prognoza_folder +config.scenario+'_'+date_str+'.tif')
		
		start_date += delta
		hour+=1
		if hour==25:
			hour=1
			dail_avg = np.nanmean(np.array(res_gem[-24:]) ,axis=0)
			if not np.isnan(dail_avg):
				daily_gem.append(dail_avg)
	
	all_res_gem = np.array(res_gem)
	all_res_gem_daily = np.array(daily_gem)
 
if len(daily_gem)==0 and os.path.isdir(prognoza_folder):
	start_date = config.subset_start_date
	end_date = config.subset_end_date
	delta = dt.timedelta(days=1)
	hour = 1
	while start_date <= end_date:
		try:
			date_str = start_date.strftime("%Y%m%d")
			arr_gem,geoTrans = load_raster(prognoza_folder+config.scenario+'_'+date_str+'.tif')
			print(date_str)
		except AttributeError:
			print('Failed to PROGNOZAopen '+prognoza_folder +config.scenario+'_'+date_str+'.tif')
		
		start_date += delta
		if not arr_gem is None:
			daily_gem.append( np.array(arr_gem))
  
	all_res_gem = np.array(res_gem)
	all_res_gem_daily = np.array(daily_gem)
	
if os.path.isdir(gauss_folder):
	start_date = config.subset_start_date
	end_date = config.subset_end_date
	delta = dt.timedelta(hours=1)
	hour = 1
	while start_date <= end_date:
		date_str = start_date.strftime("%Y%m%d_")
		READ_ERROR=True
		try:
			arr_gauss,geoTrans = load_raster(gauss_folder +config.scenario+'_'+date_str+str(hour).zfill(2)+'.tif')
			#arr_gauss,geoTrans = load_raster(gauss_folder +'_'+date_str+str(hour)+'.tif')
			res_gauss.append(arr_gauss)
			READ_ERROR=False
		except AttributeError:
			print('Failed to Gaussopen '+gauss_folder +config.scenario+'_'+date_str+str(hour).zfill(2)+'.tif')
	
			pass
		if READ_ERROR:
			try:
				arr_gauss,geoTrans = load_raster(gauss_folder +config.scenario+'_'+date_str+str(hour)+'.tif')
				res_gauss.append(arr_gauss)
				READ_ERROR=False
			except AttributeError:				
				pass
		if READ_ERROR:
			try:
				arr_gauss,geoTrans = load_raster(gauss_folder +config.scenario+'_'+date_str+str(hour).zfill(2)+'.tif')
				res_gauss.append(arr_gauss)
				READ_ERROR=False
			except AttributeError:				
				print('Failed to Gaussopen '+gauss_folder+config.scenario+'/' +config.scenario+'_'+date_str+str(hour)+'.tif')
				print('Failed to Gaussopen '+gauss_folder+'/' +config.scenario+'_'+date_str+str(hour)+'.tif')
				print('Failed to Gaussopen '+gauss_folder+'/' +config.scenario+'_'+date_str+str(hour).zfill(2)+'.tif')

		start_date += delta
		hour+=1
		if hour==25:
			hour=1
			daily_gauss.append( np.nanmean(np.array(res_gauss[-24:]),axis=0))
			
	all_res_gauss = np.array(res_gauss)
	all_res_gauss_daily = np.array(daily_gauss)




if os.path.isdir(gauss_folder):
	print('Calculating statistics... for gauss this may take a while')
	res_mean_gauss = all_res_gauss.mean(axis=0)
	CreateGeoTiff(output_folder+config.scenario+'_mean_gauss' +sufix,res_mean_gauss,geoTrans,UTM=config.UTM_zone)
	res_p902_gauss = np.percentile(all_res_gauss,90.2,axis=0)
	CreateGeoTiff(output_folder+config.scenario+'_p902_gauss' +sufix,res_p902_gauss,geoTrans,UTM=config.UTM_zone)
	res_d50_gauss = np.nansum(all_res_gauss_daily>50.0,axis=0)
	CreateGeoTiff(output_folder+config.scenario+'_d50_gauss' +sufix,res_d50_gauss,geoTrans,UTM=config.UTM_zone)

if os.path.isdir(prognoza_folder):
	print('Calculating statistics... for GEM this may take a while')
	if len(all_res_gem)>0:
		res_mean_gem = all_res_gem.mean(axis=0)
		CreateGeoTiff(output_folder+config.scenario+'_mean_gem' +sufix,res_mean_gem,geoTrans,UTM=config.UTM_zone)
		res_p902_gem = np.percentile(all_res_gem,90.2,axis=0)
		CreateGeoTiff(output_folder+config.scenario+'_p902_gem' +sufix,res_p902_gem,geoTrans,UTM=config.UTM_zone)
	elif len(all_res_gem_daily)>0:
		res_mean_gem = all_res_gem_daily.mean(axis=0)
		CreateGeoTiff(output_folder+config.scenario+'_mean_gem' +sufix,res_mean_gem,geoTrans,UTM=config.UTM_zone)
		res_p902_gem = np.percentile(all_res_gem_daily,90.2,axis=0)
		CreateGeoTiff(output_folder+config.scenario+'_p902_gem' +sufix,res_p902_gem,geoTrans,UTM=config.UTM_zone)
	if len(all_res_gem_daily)>0:
		res_d50_gem = np.nansum(all_res_gem_daily>50.0,axis=0)	
		CreateGeoTiff(output_folder+config.scenario+'_d50_gem'+sufix ,res_d50_gem,geoTrans,UTM=config.UTM_zone)
		
try:
	if os.path.isdir(rf_folder):
		print('Calculating statistics... for RF this may take a while')
		res_mean_rf = all_res_rf.mean(axis=0)
		CreateGeoTiff(output_folder+config.scenario+'_mean_rf'+sufix ,res_mean_rf,geoTrans,UTM=config.UTM_zone)
		res_p902_rf = np.percentile(all_res_rf,90.2,axis=0)
		CreateGeoTiff(output_folder+config.scenario+'_p902_rf' +sufix,res_p902_rf,geoTrans,UTM=config.UTM_zone)
		res_d50_rf = np.nansum(all_res_rf_daily>50.0,axis=0)
		CreateGeoTiff(output_folder+config.scenario+'_d50_rf' +sufix,res_d50_rf,geoTrans,UTM=config.UTM_zone)
		print('Sucessfully generated aggreagtes for HOURLY results')
except:
    print('Problem generating aggregates for HOURLY results')
    
try:
	if os.path.isdir(rf_folder_daily):
		print('Calculating statistics... for RF daily this may take a while')
		res_mean_rf = all_res_rf_daily2.mean(axis=0)
		CreateGeoTiff(output_folder+config.scenario+'_mean_rf_daily'+sufix ,res_mean_rf,geoTrans,UTM=config.UTM_zone)
		res_p902_rf = np.percentile(all_res_rf_daily2,90.2,axis=0)
		CreateGeoTiff(output_folder+config.scenario+'_p902_rf_daily' +sufix,res_p902_rf,geoTrans,UTM=config.UTM_zone)
		res_d50_rf = np.nansum(all_res_rf_daily2>50.0,axis=0)
		CreateGeoTiff(output_folder+config.scenario+'_d50_rf_daily' +sufix,res_d50_rf,geoTrans,UTM=config.UTM_zone)
		print('Sucessfully generated aggreagtes for DAILY results')
except IndexError:
    print('Problem generating aggregates for DAILY results')






