from netCDF4 import Dataset
import time
import numpy as np

def prepare_nc_output_3d(nc_filename,grid_x,grid_y,v_name,v_long_desc,no_levels):
	#function prepares auxiliary records of ncfile

	lats = grid_y
	lons = grid_x
	
	nc_output = Dataset(nc_filename,'w',format='NETCDF3_CLASSIC')

	nc_output.description = 'Converted from RPN file'
	nc_output.history = 'Created ' + time.ctime(time.time())
	nc_output.source = 'IOS-PIB, Poland'

	nc_output.createDimension('time',None)
	nc_output.createDimension('level', no_levels)
	nc_output.createDimension('longitude', grid_x.shape[0])
	nc_output.createDimension('latitude', grid_x.shape[1])

	nc_output.createVariable('time', np.float64, ('time',))
	nc_output['time'].units = 'hours since 1900-01-01 00:00:0.0' 
	nc_output['time'].calendar = 'gregorian'
	nc_output['time'].long_name = "time"
	nc_output['time'][:] = range(24)

	nc_output.createVariable('longitude', 'float32', ('longitude',))
	nc_output['longitude'][:] = np.unique(lons)
	nc_output.createVariable('latitude','float32', ('latitude',))
	nc_output['latitude'][:] = np.flip(np.unique(lats))
	
	nc_output.createVariable(v_name, 'float32',('time','level','latitude','longitude'))
	nc_output[v_name].long_name = v_long_desc

	return nc_output

def prepare_nc_output_sfc_ts(nc_filename,grid_x,grid_y,v_name,v_long_desc,no_ts):
	#function prepares auxiliary records of ncfile

	lats = grid_y
	lons = grid_x
	
	nc_output = Dataset(nc_filename,'w',format='NETCDF3_CLASSIC')

	nc_output.description = 'Converted from RPN file'
	nc_output.history = 'Created ' + time.ctime(time.time())
	nc_output.source = 'IOS-PIB, Poland'

	nc_output.createDimension('time',None)
	nc_output.createDimension('level', 1)
	nc_output.createDimension('longitude', grid_x.shape[0])
	nc_output.createDimension('latitude', grid_x.shape[1])

	nc_output.createVariable('time', np.float64, ('time',))
	nc_output['time'].units = 'hours since 1900-01-01 00:00:0.0' 
	nc_output['time'].calendar = 'gregorian'
	nc_output['time'].long_name = "time"
	nc_output['time'][:] = range(no_ts)

	nc_output.createVariable('longitude', 'float32', ('longitude',))
	nc_output['longitude'][:] = np.unique(lons)
	nc_output.createVariable('latitude','float32', ('latitude',))
	nc_output['latitude'][:] = np.flip(np.unique(lats))
	
	nc_output.createVariable(v_name, 'float32',('time','level','latitude','longitude'))
	nc_output[v_name].long_name = v_long_desc

	return nc_output

def add_variable(nc_output,v_name,v_long_desc):
	nc_output.createVariable(v_name, 'float32',('time','level','latitude','longitude'))
	nc_output[v_name].long_name = v_long_desc


def prepare_nc_output_2d(nc_filename,grid_x,grid_y,v_name,v_long_desc):
	#function prepares auxiliary records of ncfile

	lats = grid_y
	lons = grid_x
	
	nc_output = Dataset(nc_filename,'w',format='NETCDF3_CLASSIC')

	nc_output.description = 'Converted from RPN file'
	nc_output.history = 'Created ' + time.ctime(time.time())
	nc_output.source = 'IOS-PIB, Poland'

	nc_output.createDimension('time',None)
	nc_output.createDimension('longitude', grid_x.shape[0])
	nc_output.createDimension('latitude', grid_x.shape[1])
	

	nc_output.createVariable('longitude', 'float32', ('longitude',))
	nc_output['longitude'][:] = np.unique(lons)
	nc_output.createVariable('latitude','float32', ('latitude',))
	nc_output['latitude'][:] = np.flip(np.unique(lats))
	nc_output.createVariable('level', np.int32, ('level',))
	nc_output['level'][:]= range(28)

	nc_output.createVariable(v_name, 'float32',('time','level','latitude','longitude'))
	nc_output[v_name].long_name = v_long_desc

	return nc_output

def prepare_nc_output_2d_flat(nc_filename,grid_x,grid_y,v_name,v_long_desc):
	#function prepares auxiliary records of ncfile

	lats = grid_y
	lons = grid_x
	
	nc_output = Dataset(nc_filename,'w',format='NETCDF3_CLASSIC')

	nc_output.description = 'Converted from RPN file'
	nc_output.history = 'Created ' + time.ctime(time.time())
	nc_output.source = 'IOS-PIB, Poland'

	
	nc_output.createDimension('time',None)
	nc_output.createDimension('longitude', grid_x.shape[0])
	nc_output.createDimension('latitude', grid_y.shape[0])
	

	nc_output.createVariable('longitude', 'float32', ('longitude',))
	nc_output['longitude'][:] = np.unique(lons)
	nc_output.createVariable('latitude','float32', ('latitude',))
	nc_output['latitude'][:] = np.flip(np.unique(lats))
	

	nc_output.createVariable(v_name, 'float32',('time','latitude','longitude'))
	nc_output[v_name].long_name = v_long_desc

	return nc_output

def save_variable(nc_output,values,variable_name,level=0):
	nc_output[variable_name][level,:,:] = values

def save_variableTS(nc_output,values,variable_name,timestep,level):
	nc_output[variable_name][timestep,level,:,:] = values	
