#!/usr/bin/env python

import pandas as pd 
import matplotlib.pyplot as plt
from utils.config import ScenarioConfig
import datetime as dt
import argparse
from utils.data_loaders import *
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.tree import export_graphviz
from sklearn.tree import plot_tree

import pydot
import numpy as np
import _pickle as cPickle
import os 

'''
skrypt do stosowania ML
'''
plt.style.use('ggplot')
parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
config.load_ML_config()
os.makedirs(config.tif_daily_ML_dir,exist_ok=True)

sufix=''
if config.include_dow:
	sufix+='_dow'
else:
	sufix+=''
	
if config.include_meteo_obs:
	sufix+='_meteo'
else:
	sufix+=''

if config.aggregation=='max':
	sufix+='_max'
elif config.aggregation=='mean':
	sufix+='_mean'
elif config.aggregation=='median':
	sufix+='_median'
	
prefix='rf_model'


with open(config.models_dir+prefix+sufix+'_'+config.scenario+'_'+\
	config.subset_start_date.strftime("%Y%m%d")+'_'+config.subset_end_date.strftime("%Y%m%d")+'.pickle', 'rb') as f:
	rf = cPickle.load(f)

delta = dt.timedelta(days=1)

'''
Ładowanie danych meteo ze stacji
'''

df_tt = pd.read_csv('csv_input/tt_bielsko2021.csv')
df_tt.set_index(pd.to_datetime(df_tt['czas']),inplace=True)
df_vm = pd.read_csv('csv_input/vm_bielsko2021.csv')
df_vm.set_index(pd.to_datetime(df_vm['czas']),inplace=True)
df_vd = pd.read_csv('csv_input/vd_bielsko2021.csv')
df_vd.set_index(pd.to_datetime(df_vd['czas']),inplace=True)

'''
Ładowanie danych meteo
'''

hour_ts=[]
is_weekend_ts=[]

dTdz28=[]
temp28=[]
vm=[]
vd=[]

start_date = config.subset_start_date
start_date += delta

while start_date <= config.subset_end_date:
	date_str = start_date.strftime("%Y%m%d")
	print(date_str)	
	
	#arr_Pasquill,geoTrans = load_raster(config.tif_hourly_dir+config.scenario+'_'+date_str+'.tif')
	arr_GEM,geoTrans = load_raster(config.gem_tif_dir+'/'+config.scenario+'_'+date_str+'.tif')
	arr_Pasquill,geoTrans = load_raster(config.tif_daily_dir+config.scenario+'_'+date_str+'.tif')
	#arr_GEM,geoTrans = load_raster24h(config.gem_tif_dir+'/',config.scenario,date_str)
	
	

	#czas	
	hour = start_date.hour
	if start_date.weekday() in (5,6):
		is_weekend=1
	else:
		is_weekend=0
	month = start_date.month

	size = arr_GEM.flatten().shape[0]
	try:
		if config.include_dow:
			if config.include_meteo_obs:
					aa = rf.predict(list(zip(arr_Pasquill.flatten(),\
									arr_GEM.flatten(),\
									hour*np.ones(size),\
									is_weekend*np.ones(size),\
									month*np.ones(size),\
									df_tt.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vm.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vd.loc[start_date]['monitoring_value']*np.ones(size)\
									)))
			
			else:
				aa = rf.predict(list(zip(arr_Pasquill.flatten(),\
									arr_GEM.flatten(),\
									hour*np.ones(size),\
									is_weekend*np.ones(size),\
									month*np.ones(size)
										)))
		else:
			if config.include_meteo_obs:
				aa = rf.predict(list(zip(arr_GEM.flatten(),\
									arr_Pasquill.flatten(),\
									df_tt.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vm.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vd.loc[start_date]['monitoring_value']*np.ones(size)\
									)))
			else:
				aa = rf.predict(list(zip(arr_Pasquill.flatten(),arr_GEM.flatten())))

		arr_prediction = aa.reshape(arr_Pasquill.shape)
		#arr_prediction = arr_prediction * gem_std + gem_mean

		CreateGeoTiff(config.tif_daily_ML_dir +config.scenario+'_'+date_str ,arr_prediction,geoTrans,UTM=config.UTM_zone)
		print(start_date)
	except KeyError:
		print('Error processing '+str(start_date))
	
	start_date += delta