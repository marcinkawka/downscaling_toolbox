from data_loaders import *
import matplotlib.pyplot as plt
import datetime as dt
import argparse
from config import ScenarioConfig
import pydot
import numpy as np
import os 

'''
Skrypt do rysowania wykresów porównujących wynik ze stacją
'''
parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
config.load_ML_config()

start_date = config.subset_start_date
end_date = config.subset_end_date
png_dir = config.png_diags_dir
target = config.train_stations[0]
os.makedirs(png_dir,exist_ok=True)
plt.style.use('ggplot')
df_rf = pd.read_csv(config.csv_output_dir+'RF_'+config.scenario+'_'+config.train_stations[0]+'_'+start_date.strftime('%Y%m%d')+'_'+end_date.strftime('%Y%m%d')+'.csv')
df_rf.set_index(['Unnamed: 0'],inplace=True)
stations =df_rf.columns

df_gios = pd.read_csv('csv_input/pomiaryPM10_'+str(config.start_date.year)+'.csv')
df_gios = df_gios.drop_duplicates(subset=['to_timestamp','station_code']).pivot(columns=['station_code'],index=['to_timestamp'])
df_rf.set_index(pd.to_datetime(df_rf.index),inplace=True)
df_gios.set_index(pd.to_datetime(df_gios.index),inplace=True)
df_gios.columns = ["".join(x[1]) for x in df_gios.columns]

for st in stations:
    try:
        fig = plt.figure(figsize=(25,10))
        ax = fig.add_subplot()
        ax = df_gios[st].plot(ax=ax,linewidth=1.5)
        ax = df_rf[st].plot(ax=ax,linewidth=1.5)
        plt.xlim([df_rf.index[0],df_rf.index[-1]])
        plt.title(st+' observation and RandomForest')
        plt.legend(['GIOŚ observations','RandomForest'])
        plt.xlabel('datetime')
        plt.ylabel('PM10 concentration')
        plt.savefig(png_dir+st+'_target='+target+'.png',dpi=150,bbox_inches='tight')
        plt.close()
        print(st)
    except KeyError:
        pass
    
df_rf1d =  df_rf.groupby(pd.Grouper(freq="1D")).mean().copy()
df_gios1d =  df_gios.groupby(pd.Grouper(freq="1D")).mean().copy()

for st in stations:
    try:
        fig = plt.figure(figsize=(25,10))
        ax = fig.add_subplot()
        ax = df_gios1d[st].plot(ax=ax,linewidth=1.5)
        ax = df_rf1d[st].plot(ax=ax,linewidth=1.5)
        plt.xlim([df_rf1d.index[0],df_rf1d.index[-1]])
        plt.title(st+' observation and RandomForest daily averaged')
        plt.legend(['GIOŚ observations','RandomForest'])
        plt.xlabel('datetime')
        plt.ylabel('PM10 concentration')
        plt.savefig(png_dir+st+'_target='+target+'_1DAVG.png',dpi=150,bbox_inches='tight')
        plt.close()
        print(st)
    except KeyError:
        pass