#!/usr/bin/env python

from utils.data_loaders import *
import matplotlib.pyplot as plt
import datetime as dt
import argparse
from utils.config import ScenarioConfig
import pydot
import numpy as np
import os 


'''
Skrypt do wyciągania serii czasowej w danym punkcie z katalogu z godzinowymi tiffami
Wersja z więcej niż 1 stacją na obszarze obliczeń
'''


parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
config.load_ML_config()


start_date = config.subset_start_date
end_date = config.subset_end_date


def extract_stacja(x_UTM,y_UTM,start_date ,end_date):
    ts=[]
    czas=[]
    delta = dt.timedelta(days=1)

    while start_date <= end_date:
        date = start_date.strftime("%Y%m%d")
        rok = start_date.year
        miesiac = start_date.month
        day = start_date.day
        try:
            
            
            filename = config.tif_daily_ML_dir+config.scenario+"_"+date+'.tif'
            arr,geoTrans = load_raster(filename)
            
            x,y = world2Pixel(geoTrans,x_UTM,y_UTM)
            c = arr[y,x]
            ts.append(c)
            try:
                czas_pomiaru = dt.datetime(rok,miesiac,day,0,0)
            except ValueError:
                czas_pomiaru = dt.datetime(rok,miesiac,day,0,0)
                czas_pomiaru += dt.timedelta(days=1)
                
            czas.append(czas_pomiaru)
        except Exception as e:
            print('error processing '+filename)
            print(e)
            pass
        
        start_date += delta
        #print(start_date.strftime("%Y%m%d"))

    df1 = pd.DataFrame(data=ts, index=czas)
    return df1

stacje=config.pm10_stations
try:
    df_stacje = pd.read_csv('csv_input/wszystkie_stacje.csv',sep=';')
    df_stacje = df_stacje.set_index(['station_code'])
except KeyError:
    df_stacje = pd.read_csv('csv_input/wszystkie_stacje.csv',sep=',')
    df_stacje = df_stacje.set_index(['station_code'])
wybrane_stacje =pd.DataFrame(df_stacje.loc[stacje][['station_lat','station_long']])
wybrane_stacje['x_UTM']=0
wybrane_stacje['y_UTM']=0

for stacja in wybrane_stacje.iterrows():
    st_code = stacja[0]
    wybrane_stacje.loc[st_code,['x_UTM','y_UTM']]=lonlat2UTM(stacja[1]['station_long'],stacja[1]['station_lat'])


res = pd.DataFrame()
for stacja in wybrane_stacje.iterrows():
    print(stacja[0])
    df1 = extract_stacja(stacja[1]['x_UTM'],stacja[1]['y_UTM'],start_date,end_date)
    res[stacja[0]]=df1[0]

res.to_csv(config.csv_output_dir+'RF_'+config.scenario+'_'+\
    config.train_stations[0]+'_'+start_date.strftime('%Y%m%d')+'_'+end_date.strftime('%Y%m%d')+\
        'DAILY.csv',float_format='%.3f')
