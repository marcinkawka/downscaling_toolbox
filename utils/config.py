"""Load configuration from .ini file."""
import configparser
import datetime as dt
import os 

global R #rotation matrix
global R_inv #inverse rotation matrix
global C_tmp #resulting raster
global wsp_R  #rotated coordinates
global dx,dy #spatial resolution
global s_v,s_w #sigma_v,sigma_w

class ScenarioConfig:
	def __init__(self) -> None:
		self.cfg = configparser.ConfigParser()                                     

	def __init__(self,scenario_file) -> None:
		self.cfg = configparser.ConfigParser()                                     
		self.cfg.read(scenario_file)


	def load_config(self) -> None:
		# parsing config.ini file starts here
		
		self.start_date = dt.datetime.strptime(self.cfg['GENERAL']['start_day'],'%Y%m%d')
		self.end_date = dt.datetime.strptime(self.cfg['GENERAL']['end_day'],'%Y%m%d')
		
		self.scenario = str(self.cfg['GENERAL']['scenario_name'])
		self.UTM_zone  = int(self.cfg['GENERAL']['UTM_zone'])

		self.ul_lat = float(self.cfg['COMPUTATIONAL_DOMAIN']['ul_lat'])
		self.ul_lon = float(self.cfg['COMPUTATIONAL_DOMAIN']['ul_lon'])
		self.lr_lat = float(self.cfg['COMPUTATIONAL_DOMAIN']['lr_lat'])
		self.lr_lon = float(self.cfg['COMPUTATIONAL_DOMAIN']['lr_lon'])
		self.grid_size = int(self.cfg['COMPUTATIONAL_DOMAIN']['grid_size'])

		self.point_sources_per_sector = int(self.cfg['EMISSIONS']['point_sources_per_sector'])

		self.T_spalin = {'industrial':273.15+float(self.cfg['STACKS']['temp_industrial']),\
				'domest':273.15+float(self.cfg['STACKS']['temp_domest']),
				'traffic':273.15+float(self.cfg['STACKS']['temp_traffic'])}

		self.d_komina = {'industrial':float(self.cfg['STACKS']['diameter_industrial']),\
				'domest':float(self.cfg['STACKS']['diameter_domest']),\
				'traffic':float(self.cfg['STACKS']['diameter_traffic'])}

		self.v_spalin = {'industrial':float(self.cfg['STACKS']['fumes_velocity_industrial']),\
			'domest':float(self.cfg['STACKS']['fumes_velocity_domest']),\
			'traffic':float(self.cfg['STACKS']['fumes_velocity_traffic'])}

		self.domest_shp =str(self.cfg['EMISSIONS']['domest_shp'])
		self.industrial_shp =str(self.cfg['EMISSIONS']['industrial_shp'])
		self.traffic_shp =str(self.cfg['EMISSIONS']['traffic_shp'])
		self.emissions_grid_size_x = int(self.cfg['EMISSIONS']['emissions_grid_size_x'])
		self.emissions_grid_size_y = int(self.cfg['EMISSIONS']['emissions_grid_size_y'])
		self.emissions_reduction_factor_kombyt = float(self.cfg['EMISSIONS']['emissions_reduction_factor_kombyt'])
		self.emissions_reduction_factor_traffic = float(self.cfg['EMISSIONS']['emissions_reduction_factor_traffic'])
		self.emissions_reduction_factor_industrial = float(self.cfg['EMISSIONS']['emissions_reduction_factor_industrial'])
		#some optional parameters
		try:
			self.emiter_height_traffic = float(self.cfg['EMISSIONS']['emiter_height_traffic'])
		except Exception:
			print("Emiter height not found")
		try:
			self.emission_field = self.cfg['EMISSIONS']['emission_field']
			self.emission_H = self.cfg['EMISSIONS']['emission_h_field']
			print('Will simulate '+self.emission_field)
		except Exception:
			print("Default PM10 simulation")
			self.emission_field = 'PM10_KG'
			self.emission_H = 'EMIT_H'
   
		#meteo
		self.wind_path = str(self.cfg['INPUT']['wind_path'])
		self.Ri_folder  = str(self.cfg['INPUT']['Ri_folder'])
		self.meteo_folder = str(self.cfg['INPUT']['meteo_folder'])
		
		#dirs
		self.work_dir = self.cfg['DIRS']['work_dir']
		self.rpn_dir = self.cfg['DIRS']['rpn_dir']
		self.tif_daily_dir = self.cfg['DIRS']['tif_daily_dir']
		self.tif_hourly_dir = self.cfg['DIRS']['tif_hourly_dir']
		self.tif_monthly_dir = self.cfg['DIRS']['tif_monthly_dir']
		self.png_diags_dir = self.cfg['DIRS']['png_diags_dir'] 
		self.models_dir = self.cfg['DIRS']['models_dir']   
		self.tif_hourly_ML_dir= self.cfg['DIRS']['tif_hourly_ML_dir'] 
		self.tif_daily_ML_dir= self.cfg['DIRS']['tif_daily_ML_dir']   
		self.tif_final_dir = self.cfg['DIRS']['tif_final_dir']  
		self.gem_tif_dir = self.cfg['DIRS']['gem_tif_dir'] 
		self.csv_output_dir = self.cfg['DIRS']['csv_output_dir'] 
		os.makedirs(self.png_diags_dir,exist_ok =True)
		os.makedirs(self.work_dir,exist_ok =True)
		os.makedirs(self.tif_daily_dir,exist_ok =True)
		os.makedirs(self.tif_hourly_dir,exist_ok =True)
		os.makedirs(self.tif_monthly_dir,exist_ok =True)
		os.makedirs(self.models_dir,exist_ok =True)
		os.makedirs(self.tif_hourly_ML_dir,exist_ok =True)
		os.makedirs(self.tif_daily_ML_dir,exist_ok =True)
		os.makedirs(self.tif_final_dir,exist_ok =True)
		os.makedirs(self.gem_tif_dir,exist_ok =True)
		os.makedirs(self.csv_output_dir,exist_ok =True)
		  
		#output
		self.png_hourly_output = self.cfg.getboolean('OUTPUT','png_hourly_output')
		self.png_daily_output = self.cfg.getboolean('OUTPUT','png_daily_output')
		self.png_monthly_output = self.cfg.getboolean('OUTPUT','png_monthly_output')
		self.tif_hourly_output = self.cfg.getboolean('OUTPUT','tif_hourly_output')
		self.tif_daily_output = self.cfg.getboolean('OUTPUT','tif_daily_output')
		self.tif_monthly_output = self.cfg.getboolean('OUTPUT','tif_monthly_output')
	
		#observations
		pm10_stations = self.cfg['OBSERVATIONS']['pm10_stations'] 
		self.pm10_stations = pm10_stations.split(',')

		self.observations_file = self.cfg['OBSERVATIONS']['observations_file']
		self.meteo_observations_file = self.cfg['OBSERVATIONS']['meteo_observations_file']
		self.gem_file = self.cfg['GEM']['gem_file']
  
	def load_ML_config(self) -> None:
		self.subset_start_date = dt.datetime.strptime(self.cfg['MACHINE_LEARNING']['subset_start_date'],'%Y%m%d')
		self.subset_end_date = dt.datetime.strptime(self.cfg['MACHINE_LEARNING']['subset_end_date'],'%Y%m%d')
		train_stations = self.cfg['MACHINE_LEARNING']['train_stations']
		test_stations = self.cfg['MACHINE_LEARNING']['test_stations'] 	
		meteo_stations = self.cfg['OBSERVATIONS']['meteo_stations'] 
		self.train_stations = train_stations.split(',')
		self.test_stations = test_stations.split(',')
		self.meteo_stations = meteo_stations.split(',')
		self.include_dow = self.cfg.getboolean('MACHINE_LEARNING','include_dow')
		self.include_meteo_obs = self.cfg.getboolean('MACHINE_LEARNING','include_meteo_obs')
		self.test_set_size = self.cfg.getfloat('MACHINE_LEARNING','test_set_size')
		self.aggregation = self.cfg['MACHINE_LEARNING']['aggregation']
  
  
	def create_dirs(self) -> None:
		os.makedirs(self.work_dir, exist_ok=True)
		os.makedirs(self.tif_hourly_dir, exist_ok=True)
		os.makedirs(self.tif_monthly_dir, exist_ok=True)
		os.makedirs(self.tif_daily_dir, exist_ok=True)
		os.makedirs(self.tif_hourly_dir, exist_ok=True)
		os.makedirs(self.tif_daily_dir, exist_ok=True)
		os.makedirs(self.csv_output_dir, exist_ok=True)

	'''
	config.read('settings/config.ini')
	config.dimX = 0
	config.dimY = 0

	#old settings (to be discussed)
	config.T_spalin = {'industrial':73 + 273.15,'domest':50+273.15,'traffic':200+273.15}
	config.d_komina = {'industrial':2.5,'domest':0.5,'traffic':0.1}
	config.v_spalin = {'industrial':5,'domest':0.05,'traffic':0.01}

	#dla siekierek
	#config.V_spalin={1:15.14,2:15.18,3:13.37,4:11.40,5:7.63,6:6.10,7:5.27,8:5.18,9:6.90,10:11.44,11:14.94,12:14.55}
	'''
	