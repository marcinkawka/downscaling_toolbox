import numpy as np
from pyproj import Transformer
import asyncio

def background(f):
    def wrapped(*args, **kwargs):
        return asyncio.get_event_loop().run_in_executor(None, f, *args, **kwargs)

    return wrapped

def pixel2World(geoMatrix, x, y):
  ulX = geoMatrix[0]
  ulY = geoMatrix[3]

  xDist = geoMatrix[1]
  yDist = geoMatrix[5]

  rtnX = geoMatrix[2]
  rtnY = geoMatrix[4]
  
  pixel = int((x *  xDist + ulX))
  line = int(( y * yDist +ulY))
  return (pixel, line)

def world2Pixel(geoMatrix, x, y):
  ulX = geoMatrix[0]
  ulY = geoMatrix[3]
  xDist = geoMatrix[1]
  yDist = geoMatrix[5]
  rtnX = geoMatrix[2]
  rtnY = geoMatrix[4]
  pixel = int((x - ulX) / xDist)
  line = int((y - ulY) / yDist)
  return (pixel, line) 

def calculate_avg_subraster(array,gt,ul_lat,ul_lon,lr_lat,lr_lon):
  x1,y1 = world2Pixel(gt,ul_lon,ul_lat)   
  x2,y2 = world2Pixel(gt,lr_lon,lr_lat)
  return np.mean(array[y1:y2,x1:x2])

def lonlat2UTM(lon,lat,UTM=None):
  if UTM is None:
    if lon<18:
      zone = "EPSG:32633"
    else:
      zone = "EPSG:32634"
  else:
    if UTM==33:
      zone = "EPSG:32633"
    elif UTM==34:
      zone = "EPSG:32634"
  tt=Transformer.from_crs("EPSG:4326",zone)
  
  return tt.transform(lat,lon)

def UTM2lonlat(x,y,zone):
  if zone=='33N':
    tt=Transformer.from_crs("EPSG:32633","EPSG:4326")
  elif zone=='34N':
    tt=Transformer.from_crs("EPSG:32634","EPSG:4326")

  return tt.transform(x,y)