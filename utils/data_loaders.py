import numpy as np
from osgeo import gdal
from osgeo import osr
import geopandas as gpd
import random
import calendar
from skimage.measure import block_reduce
#from calculations import *
from utils.other_tools import calculate_avg_subraster,world2Pixel,lonlat2UTM

import pprint as pp
import datetime as dt
import pandas as pd

gdal.PushErrorHandler('CPLQuietErrorHandler')

def load_raster(tif_file):
	'''
	simple function for single raster loading
	'''
	ds = gdal.Open(tif_file)
	band = ds.GetRasterBand(1)
	stats = band.GetStatistics( True, True )
	arr = band.ReadAsArray()
	geoTrans = ds.GetGeoTransform()

	return arr,geoTrans

def load_raster24h(folder, file_prefix, date_str):
	'''
	function for loading 24 rasters and averaging them
	'''
	i=0
	for hh in range(24):
		try:
			tif_file = folder + file_prefix + '_' +date_str+ str(hh).zfill(2)+'00.tif'
			ds = gdal.Open(tif_file)
			band = ds.GetRasterBand(1)
			stats = band.GetStatistics( True, True )
			geoTrans = ds.GetGeoTransform()		
			if hh==0:
				arr = band.ReadAsArray()
			else:
				arr += band.ReadAsArray()
			i+=1
		except AttributeError:
			print("Error loading "+tif_file)		
	return arr/i,geoTrans


def CreateGeoTiff(Name, Array,geoTrans=None,UTM=None):
	DataType = gdal.GDT_Float32
	driver = gdal.GetDriverByName('GTiff')
	NewFileName = Name+'.tif'
	# Set up the dataset
	DataSet = driver.Create( NewFileName, Array.shape[1], Array.shape[0], 1, DataType )
	DataSet.SetGeoTransform(geoTrans)          
	srs = osr.SpatialReference()
	if UTM is None:
		zone = 32634
	else:
		if UTM==33:
			zone = 32633
		elif UTM==34:
			zone = 32634

	#srs.ImportFromEPSG(32634) #UTM!
	srs.ImportFromEPSG(zone) #UTM!
	DataSet.SetProjection(srs.ExportToWkt())          
	DataSet.GetRasterBand(1).WriteArray( Array )
	DataSet.FlushCache() 
	
	return NewFileName

def load_traffic(cfg):
	'''
		Loading traffic emissions
	'''
	shapefile = gpd.read_file(cfg.traffic_shp)
	shapefile['x']=shapefile.geometry.x
	shapefile['y']=shapefile.geometry.y

	subset = shapefile[['x','y','PM10_kg']]
	tuples = [tuple(x) for x in subset.values]
	print("There are "+str(len(tuples))+" traffic sources")
	return tuples


def load_industrial(cfg):
	'''
		Loading industrial emissions
	'''
	shapefile = gpd.read_file(cfg.industrial_shp)
	#print(shapefile.columns)
	try:
		shapefile['H'] = shapefile[cfg.emission_H]
		#in case of empty H
		emptyH = (shapefile['H']==0.0)
		shapefile.loc[emptyH,'H']=2.5
		del shapefile[cfg.emission_H]  
		
		shapefile['x']=shapefile.geometry.x
		shapefile['y']=shapefile.geometry.y
		
		subset = shapefile[['x','y',cfg.emission_field,'H']]
		
	except KeyError:
		subset = shapefile[['x','y',cfg.emission_field,'H']]
	
	tuples = [tuple(x) for x in subset.values]
	print("There are "+str(len(tuples))+" industrial sources")
	return tuples

def load_kombyt(cfg):
	'''
	Loading domestic emissions into list of tuples
	'''
	shapefile = gpd.read_file(cfg.domest_shp)
	shapefile['x']=shapefile.geometry.x
	shapefile['y']=shapefile.geometry.y
	
	try:
		shapefile['H'] = 3.5*shapefile['LICZBAKOND'] + 0.8
		del shapefile['LICZBAKOND']  		
	except KeyError:
		shapefile['H'] = 3.5*shapefile['liczbaKond'] + 0.8
		del shapefile['liczbaKond']  		
	print('Ustawiono wysokość kominów w kombycie')
 
	sukces =0
	try:
		subset = shapefile[['x','y','Epm10','H']]
		sukces=1
	except KeyError:
		pass
	try:
		subset = shapefile[['x','y','PM10_KG','H']]
		sukces=1
	except KeyError:
		pass
	try:
		subset = shapefile[['x','y','PM10_kg','H']]
		sukces=1
	except KeyError:
		pass
	if sukces==0:
		raise KeyError("Failed to find PM10 field in kombyt attribute table!")

	tuples = [tuple(x) for x in subset.values]
	print("There are "+str(len(tuples))+" domestic sources")
	return tuples

def get_value_from_band(band,gT,cfg):
		arr = band.ReadAsArray()    
		return calculate_avg_subraster(arr,gT,cfg.ul_lat,cfg.ul_lon,cfg.lr_lat,cfg.lr_lon)

def load_ambientTemp(datestr,cfg):
	'''
	Loading ambient temp and Richardson number profile
	'''
	#stare
	#
	try:
		filename = cfg.Ri_folder +'Ri_profile_'+datestr+'.nc'
		gdal.PushErrorHandler('CPLQuietErrorHandler')
		ds_TT = gdal.Open('NETCDF:"'+filename+'":TT') 
		ds_Z = gdal.Open('NETCDF:"'+filename+'":Z') 
		gT = ds_TT.GetGeoTransform()
	except AttributeError:
		filename = cfg.Ri_folder +'temp_profile_'+datestr+'.nc' 
		gdal.PushErrorHandler('CPLQuietErrorHandler')
		ds_TT = gdal.Open('NETCDF:"'+filename+'":TT') 
		ds_Z = gdal.Open('NETCDF:"'+filename+'":Z') 
		gT = ds_TT.GetGeoTransform()
	 
	tt28_ts=[]
	tt27_ts=[]
	tt26_ts=[]
	tt25_ts=[]
	tt24_ts=[]
	tt23_ts=[]

	z28_ts=[]
	z27_ts=[]
	z26_ts=[]
	z25_ts=[]
	z24_ts=[]
	z23_ts=[]

	for ts in range(24):
			tt28_ts.append(get_value_from_band(ds_TT.GetRasterBand((ts+1)*28),gT ,cfg))
			tt27_ts.append(get_value_from_band(ds_TT.GetRasterBand((ts+1)*28-1),gT,cfg ))
			tt26_ts.append(get_value_from_band(ds_TT.GetRasterBand((ts+1)*28-2),gT,cfg ))
			tt25_ts.append(get_value_from_band(ds_TT.GetRasterBand((ts+1)*28-3),gT ,cfg))
			tt24_ts.append(get_value_from_band(ds_TT.GetRasterBand((ts+1)*28-4),gT ,cfg))
			tt23_ts.append(get_value_from_band(ds_TT.GetRasterBand((ts+1)*28-5),gT ,cfg))
			
			z28_ts.append(get_value_from_band(ds_Z.GetRasterBand((ts+1)*28),gT,cfg ))
			z27_ts.append(get_value_from_band(ds_Z.GetRasterBand((ts+1)*28-1),gT ,cfg))
			z26_ts.append(get_value_from_band(ds_Z.GetRasterBand((ts+1)*28-2),gT,cfg ))
			z25_ts.append(get_value_from_band(ds_Z.GetRasterBand((ts+1)*28-3),gT ,cfg))
			z24_ts.append(get_value_from_band(ds_Z.GetRasterBand((ts+1)*28-4),gT ,cfg))
			z23_ts.append(get_value_from_band(ds_Z.GetRasterBand((ts+1)*28-5),gT ,cfg))

	Z={28:z28_ts,27:z27_ts,26:z26_ts,25:z25_ts,24:z24_ts,23:z23_ts}
	T={28:tt28_ts,27:tt27_ts,26:tt26_ts,25:tt25_ts,24:tt24_ts,23:tt23_ts}

	return Z,T 

def load_Ri(datestr,cfg,lvl=28):
	'''
	Funkcja ładująca profil czasowy liczby Ricardsona dla danego dnia w danym obszarze
	'''
	filename = cfg.Ri_folder +'Ri_profile_'+datestr+'.nc'
	gdal.PushErrorHandler('CPLQuietErrorHandler')
	ds = gdal.Open('NETCDF:"'+filename+'":Ri') 
	
	Ri_ts=[]
	for ts in range(24):
			bd= (ts+1)*lvl
			band = ds.GetRasterBand(bd)
			arr = band.ReadAsArray()
			gT = ds.GetGeoTransform()
			Ri = calculate_avg_subraster(arr,gT,cfg.ul_lat,cfg.ul_lon,cfg.lr_lat,cfg.lr_lon)
			Ri_ts.append(Ri)

	return Ri_ts


#To silence GDAL warnings
#export CPL_LOG=/dev/null
def load_winds_ts(time_step,cfg):
	
	ds = gdal.Open('GEM_incoming/UU_ts.nc')
	band = ds.GetRasterBand(time_step)
	arr = band.ReadAsArray()
	gT = ds.GetGeoTransform()
	U_10 = calculate_avg_subraster(arr,gT,cfg.ul_lat,cfg.ul_lon,cfg.lr_lat,cfg.lr_lon)

	ds = gdal.Open('GEM_incoming/VV_ts.nc')
	band = ds.GetRasterBand(time_step)
	arr = band.ReadAsArray()
	gT = ds.GetGeoTransform()
	V_10 = calculate_avg_subraster(arr,gT,cfg.ul_lat,cfg.ul_lon,cfg.lr_lat,cfg.lr_lon)

	M_10 = np.sqrt(U_10**2+V_10**2) 
	theta = np.arctan(V_10/U_10)
	
	return (M_10,theta)

def load_winds_VM(date,time_step,cfg):
	filename = cfg.wind_path+'wind_ts'+date+'.nc'
	ds = gdal.Open('NETCDF:"'+filename+'":VM') 
	band = ds.GetRasterBand(time_step)
	arr = band.ReadAsArray()
	gT = ds.GetGeoTransform()
	VM_10 = calculate_avg_subraster(arr,gT,cfg.ul_lat,cfg.ul_lon,cfg.lr_lat,cfg.lr_lon)

	ds = gdal.Open('NETCDF:"'+filename+'":VD') 
	band = ds.GetRasterBand(time_step)
	arr = band.ReadAsArray()
	gT = ds.GetGeoTransform()
	VD_10 = calculate_avg_subraster(arr,gT,cfg.ul_lat,cfg.ul_lon,cfg.lr_lat,cfg.lr_lon)

	return (VM_10,VD_10)

def load_winds_UV(filename,time_step,cfg):

	ds = gdal.Open('NETCDF:"'+filename+'":UU') 
	band = ds.GetRasterBand(time_step)
	arr = band.ReadAsArray()
	gT = ds.GetGeoTransform()
	U_10 = calculate_avg_subraster(arr,gT,cfg.ul_lat,cfg.ul_lon,cfg.lr_lat,cfg.lr_lon)

	ds = gdal.Open('NETCDF:"'+filename+'":VV') 
	band = ds.GetRasterBand(time_step)
	arr = band.ReadAsArray()
	gT = ds.GetGeoTransform()
	V_10 = calculate_avg_subraster(arr,gT,cfg.ul_lat,cfg.ul_lon,cfg.lr_lat,cfg.lr_lon)

	M_10 = np.sqrt(U_10**2+V_10**2) 
	theta = np.arctan(V_10/U_10)
	#print('U = '+str(U_10))
	#print('V = '+str(V_10))
	#print('__________________')
	#print('M = '+str(M_10))
	#print('theta = '+str(theta))
	return (M_10,theta)


def load_temporal_profile(nc_filename,variable,steps,coordinates=None):
	ds = gdal.Open('NETCDF:"'+nc_filename+'":'+variable) 
	profile=[]
	for i in range(steps):
		band = ds.GetRasterBand(i+1)
		arr = band.ReadAsArray()
		gT = ds.GetGeoTransform()
		if coordinates is not None:
			val = calculate_avg_subraster(arr,gT,coordinates['ul_lat'],coordinates['ul_lon'],coordinates['lr_lat'],coordinates['lr_lon'])
			profile.append(val)
	return profile 

def create_arrays(cfg):
	'''
	This funcion create target array C and coordinates vectors wsp_X, wsp_Y
	Assuming grid_size in meters
	'''

	resize_factor_X = round(cfg.grid_size /cfg.emissions_grid_size_x)
	resize_factor_Y = round(cfg.grid_size /cfg.emissions_grid_size_y)

	ulX, ulY = lonlat2UTM(cfg.ul_lon,cfg.ul_lat)
	lrX, lrY = lonlat2UTM(cfg.lr_lon,cfg.lr_lat)

	dimX = round(((lrX - ulX)/cfg.emissions_grid_size_x))
	dimY = round(((ulY - lrY)/cfg.emissions_grid_size_y))
	C = np.zeros((dimY,dimX))

	
	if resize_factor_X>1:
		C = block_reduce(C, block_size=(resize_factor_X, resize_factor_Y), func=np.max)

	print("Size of raster data after resize = ",end='')
	print(C.shape)

	wsp_X = ulX + np.array(range(C.shape[1]))*cfg.grid_size 
	wsp_Y = ulY - np.array(range(C.shape[0]))*cfg.grid_size 
	cfg.dimX = wsp_X.shape[0]
	cfg.dimY = wsp_Y.shape[0]

	wsp = np.dstack(np.meshgrid(wsp_X,wsp_Y,indexing='xy')).reshape((-1, 2),order='A')
	geoTrans = (ulX,cfg.grid_size,0.0,ulY,0.0,-cfg.grid_size)
	return C, wsp,geoTrans

def load_emissions(cfg):
	liczba_zrodel = cfg.point_sources_per_sector	
	kombyt		    = load_kombyt(cfg)
	industrial 	    = load_industrial(cfg)
	traffic 	    = load_traffic(cfg)
	
	ZR_kombyt        = random.sample(list(kombyt),k=min(int(liczba_zrodel),len(kombyt)))
	ZR_industrial    = random.sample(list(industrial),k=min(int(liczba_zrodel),len(industrial)))
	ZR_traffic    	 = random.sample(list(traffic),k=min(int(liczba_zrodel),len(traffic)))
	return ZR_kombyt,ZR_industrial,ZR_traffic
	
def load_emission_profiles(date):
	'''
	Function returns traffic and domestic hourly profiles for a given date (sensetive to weekends)
	'''
	# TYMCZASOWO WKLEJONY KRAKOWSKI PROFILE dobowe, sumują sie do 24
	kombyt_profile=[0.88653386, 0.8680975, 0.8607928, 0.86156565, 0.8670683, 0.8857544, 0.95401293,\
	1.0238267, 1.0617663, 1.0786388, 1.0776411, 1.0802872, 1.0774565, 1.0747911, 1.0612508,\
	1.0558889, 1.0567985, 1.0605727, 1.065098, 1.0708177, 1.0579528, 1.01978, 0.97249365, 0.92111343]
	traffic_profile_saturday = [0.37633348, 0.25124434, 0.18728524, 0.17388932, 0.2171747, 0.3593159, 0.4620812, 0.64738476, 0.96026427, 1.3352537, 1.5666797,\
	1.6750135, 1.732751, 1.7938088, 1.8127582, 1.6775061, 1.595003, 1.5883069, 1.4911207, 1.1750656, 0.93224925, 0.7357121, 0.6693224, 0.5844753]
	traffic_profile_sunday = [0.5190793, 0.3618631, 0.24465469, 0.19927962, 0.2050054, 0.26788378,\
	0.289131, 0.3590232, 0.5778694, 0.9544549, 1.3058047, 1.5505934, 1.6778111, 1.774526,\
	1.8761418, 1.8003052, 1.7930695, 1.882231, 1.748012, 1.475218, 1.1922963, 0.89625305, 0.6387103, 0.41078323]
	traffic_profile_weekday =[0.1941053, 0.14378434, 0.12643644, 0.13665128, 0.21756424, 0.5527142, 1.1152887, 1.5085967, 1.3851038, 1.2815416,\
	1.2869843, 1.3372303, 1.4238858, 1.5302367, 1.6167436, 1.729897, 1.8832792,\
	1.7929232, 1.4747967, 1.0774245, 0.7917497, 0.59886026, 0.4721553, 0.32204586]
	#profil miesięczny emsji
	S2_m_prof = {1:0.167,2:0.1583,3:0.129167,	4:0.075,5:0.05,6:0.0125,7:0.0125,	8:0.0125,	9:0.033,	10:0.0583,	11:0.133	,12:0.1583}
	#profil godzinowy emisji
	S2_h_prof = {24:0.039583,1:0.039583,2:0.039583,3:0.039583,4:0.039583,5:0.04167,6:0.04583,7:0.04583,8:0.04583,9:0.04167,10:0.04167,11:0.04167,12:0.04167,\
		13:0.039583,14:0.039583,15:0.039583,16:0.039583,17:0.04583,18:0.04583,19:0.04583,20:0.04167,21:0.039583,22:0.039583,23:0.039583}

	liczba_dni = calendar.monthrange(date.year,date.month)[1]
	mnoznik_kombyt = np.array(kombyt_profile)/24 #znormalizowany profil dobowy
	mnoznik_kombyt = mnoznik_kombyt*S2_m_prof[date.month]/liczba_dni
	# [kg/h] -> [g/s]	
	mnoznik_kombyt = mnoznik_kombyt * 1e3 / (3600)

	#S2_m normalizowane jest do 12 miesięcy
	#dzien 0 to POniedziałEKEYEXPIRED
	#5 - SObota	
	#6 - niedziela
	if date.weekday()==6:
		mnoznik_traffic = np.array(traffic_profile_sunday)/24
	elif date.weekday()==5:
		mnoznik_traffic = np.array(traffic_profile_saturday)/24
	else:
		mnoznik_traffic = np.array(traffic_profile_weekday)/24

	mnoznik_traffic = mnoznik_traffic / 365
 	# [kg/h] -> [g/s]
	mnoznik_traffic = mnoznik_traffic * 1e3 / (3600)
 
	#mnoznik dla danej godziny o danej porze
	# [kg/rok] -> [kg/h]
	
	mnoznik_industrial = np.array(list(S2_h_prof.values()))*(S2_m_prof[date.month]/liczba_dni)
	# [kg/h] -> [g/s]
	mnoznik_industrial = mnoznik_industrial * 1e3 / (3600)

	return mnoznik_traffic,mnoznik_kombyt, mnoznik_industrial

def extract_station_ts(x_UTM,y_UTM,cfg):
	ts=[]
	czas=[]
	delta = dt.timedelta(days=1)
	start_date = cfg.start_date
 
	while start_date <= cfg.end_date:
		date = start_date.strftime("%Y%m%d")
		rok = start_date.year
		miesiac = start_date.month
		day = start_date.day

		for godzina in range(1,25):
			try:
				#obsługa dwóch konwencji nazewniczych
				try:
					filename = cfg.tif_hourly_dir+"/"+cfg.scenario+"_"+date+'_'+str(godzina)+'.tif'
					arr,geoTrans = load_raster(filename)
				except:
					filename = cfg.tif_hourly_dir+"/"+cfg.scenario+"_"+date+'_'+str(godzina).zfill(2)+'.tif'
					arr,geoTrans = load_raster(filename)
				x,y = world2Pixel(geoTrans,x_UTM,y_UTM)
				c = arr[y,x]
				ts.append(c)
				try:
					czas_pomiaru = dt.datetime(rok,miesiac,day,godzina,0)
				except ValueError:
					czas_pomiaru = dt.datetime(rok,miesiac,day,0,0)
					czas_pomiaru += dt.timedelta(days=1)
					
				czas.append(czas_pomiaru)
			except Exception as e:
				print('error processing '+filename)
				print(e)
				pass
		print(start_date.strftime("%Y%m%d"))
		start_date += delta
		

	df1 = pd.DataFrame(data=ts, index=czas)
	return pd.to_numeric(df1[0])