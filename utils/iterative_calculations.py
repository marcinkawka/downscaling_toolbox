
from utils.calculations import calculate_uplift, calculate_concentration_from_sources

from toolz.itertoolz import partition_all
import numpy as np
import multiprocessing as mp 
import math 

from multiprocessing import Pool, Lock, RawArray, Process

lock = Lock()

def iterate_sources(sources,M_10a,stab_classa,Tambient,m,type,config,mnoznik_zr =1):
	'''
	This is the default function, which iterates over point sources
	'''
	global lock
	global stab_class
	global M_10

	global grid_size
	grid_size = 250
	dx = grid_size
	dy = grid_size

	POOL_SIZE = 48
	
	stab_class = stab_classa
	M_10 = M_10a

	#split dataset into chuncs for each CPU core
	chunk_size = len(sources) // POOL_SIZE +1 
	zr_parts = list(partition_all(chunk_size, sources))
	

	#resulting array
	shared_array = mp.RawArray('d', config.C_tmp.shape[0])
	jobs=[]
	for zr_part in zr_parts:	
		p = Process(target=iterate_part, args=(shared_array,zr_part,Tambient,m,type,config,mnoznik_zr))
		jobs.append(p)
		p.start()

	for proc in jobs:
		proc.join()

	config.C_tmp  = config.C_tmp  + np.reshape(np.frombuffer(shared_array, dtype=np.float64),-1)
	print('|',end='')
 
def iterate_part(shared_array,zr_part,Tambient,m,type,config,mnoznik_zr):
	'''
	This function is run in parallel for a chunk selected from sources list
	'''
	global stab_class
	global M_10


	Ctmp = np.zeros(config.dimX*config.dimY)

	for zr in zr_part:
		try:
			(X_UTM , Y_UTM, Epm10, He) = zr
			
		except ValueError:
			(X_UTM , Y_UTM, Epm10) = zr
			#This is usually the traffic case
			He =1.5
		if type == 'traffic':
			He=config.emiter_height_traffic
   
		if not math.isnan(Epm10 ) and Epm10>0:
			if math.isnan(He):
				He =0.0

			if He>10:
				u_h = M_10*(He/10)**m
			else:
				u_h = M_10
			
			d = config.d_komina[type]
			v = config.v_spalin[type]
			Ts = config.T_spalin[type]

			Q,dH = calculate_uplift(u_h,Tambient+273.15,Ts,v,d)
			#mała emisja nie może się unosić aż tak!
			if Epm10 <10:
				dH = min(dH,He)
		
			cc = calculate_concentration_from_sources(X_UTM,Y_UTM,M_10,mnoznik_zr*Epm10,He+dH,stab_class,config)
					
			Ctmp = np.add(Ctmp, cc.astype(np.float64))  

	lock.acquire()
	Cnp_array = np.reshape( np.frombuffer(shared_array, dtype=np.float64 ),-1) 
	Cnp_array = np.add(Cnp_array, Ctmp.astype(np.float64),out=Cnp_array)  
	lock.release()
	print('.',end='')
 
 
'''
 #obsolete, not used any more
 
 def iterate_kombyt_single(ZR_kombyt,M_10a,stab_classa):
	global lock
	global stab_class
	global mnoznik
	global M_10

	mnoznik = S2_m_prof[miesiac]*S2_h_prof[godzina]
	dx = grid_size
	dy = grid_size

	
	stab_class = stab_classa
	M_10 = M_10a

	for zr in ZR_kombyt:
		(X_UTM , Y_UTM, Epm10, He)=zr
		cc = calculate_concentration_from_sources(X_UTM,Y_UTM,M_10,mnoznik*Epm10,He,stab_class)
		config.C_tmp  = np.add(config.C_tmp,cc)
	print('|',end='')
 
 def iterate_traffic(ZR_traffic,M_10,Q,geoTrans):
	licznik=1

	for zr in ZR_traffic:
		(X_UTM , Y_UTM)=pixel2World(geoTrans, zr[0], zr[1])
		calculate_concentration_UTM(X_UTM,Y_UTM,M_10,Q,0)
		licznik = licznik +1
  
'''