import numpy as np
from scipy import stats
import scipy.interpolate as interpolate
import asyncio
from multiprocessing import Pool, Lock
from sklearn.metrics import mean_squared_error

#from utils.other_tools import *

def calculate_ambient_temperature(elevation,h,config):
    '''
    Funkcja tworzy interpolator dla danej godziny h
    i podaje wartość dla wysokości elevation
    '''
    z=np.zeros(6)
    z[0] = 0
    z[1] = np.mean(config.Z[27]) - np.mean(config.Z[28])
    z[2] = np.mean(config.Z[26]) - np.mean(config.Z[28])
    z[3] = np.mean(config.Z[25]) - np.mean(config.Z[28])
    z[4] = np.mean(config.Z[24]) - np.mean(config.Z[28])
    z[5] = np.mean(config.Z[23]) - np.mean(config.Z[28])

    T_profile = [config.T[28][h-1],config.T[27][h-1],config.T[26][h-1],config.T[25][h-1],config.T[24][h-1],config.T[23][h-1]]

    f = interpolate.interp1d(z, T_profile,kind='linear')

    return f(elevation)+273.15


def get_stability_class(Ri):
    '''
    Funkcja wyznacza klasę stabilności (tab 3.5 str 133, Markiewicz 2004)
    na podstawie gradientowej liczby Ricardsona oraz wykładnik do ekstrapolacji wiatru
    '''
    if Ri<-2.038:
        return 'A',0.08 
    elif Ri<-0.75:
        return 'B',0.143 
    elif Ri<-0.18:
        return 'C',0.196 
    elif Ri<0.083:
        return 'D',0.27 
    elif Ri<0.16:
        return 'E',0.363 
    elif Ri>=0.16:
        return 'F',0.44 

def calculate_uplift(u_h,T,Ts,v,d):
    '''
    Funkcja licząca wyniesienie spalin
    u_h - prędkość wiatru na wysokości emitora [m/s]
    v - prędkość wylotowa gazów
    d - średnica komina
    T - temperatura otoczenia [K]
    Ts - temperatura spalin [K]
    '''                   
    if u_h<0.01:
        u_h=0.01            
    #Najpierw strumień ciepła (wg. ustawy)
    Q = (np.pi*d**2) /4 *273.16/Ts *1.3 *(Ts - T) * v
    #formuła Concave:
    dHc = (1.126 * Q**0.58)/(u_h)**0.7
    #formuła Hollanda
    if v<0.5*u_h:
        dHh=0
    elif v>u_h:
        dHh = (1.5*v*d*0.00974*Q)/u_h
    else:
        dHh = (1.5*v*d*0.00974*Q)/u_h*(v-0.5*u_h)/(0.5*u_h)

    #ostateczny wynór    
    if Q>24000:
        dH = dHc
    elif Q<16000:
        dH = dHh
    else:
        dH = dHh *(24000-Q)/8000 +dHc * (Q-16000)/8000

    return Q,dH
    
def calculate_sysz(X,stab_class):
    '''
    Schemat Briggsa (na podstawie Markiewicz 2004, str. 157)
    '''
    #warunki miejskie
    if stab_class=='A' or stab_class=='B':
        k11=.32
        k22=.0004
        k33=-0.5
        k44=.24
        k55=.001
        k66=0.5
    elif stab_class=='C':
        k11=.22
        k22=.0004
        k33=-0.5
        k44=.2
        k55=0
        k66=0
    elif stab_class=='D':
        k11=.16
        k22=.0004
        k33=-0.5
        k44=.14
        k55=.0003
        k66=-0.5
    elif stab_class=='E' or stab_class=='F':
        k11=.11
        k22=.0004
        k33=-0.5
        k44=.08
        k55=.00015
        k66=-0.5
    
    #równania 3.33 i 3.44 z Markiewicz 2004
    s_y = k11*X*(1+k22*X)**k33
    s_z = k44*X*(1+k55*X)**k66
    return s_y,s_z


def calculate_concentration_from_sources(X,Y,M_10,Em,H_e,stab_class,config):
    '''
    This function is run for each source (stack)
    (X,Y) - source location
    Em - emission magnitude (max hourly emission)
    M_10 - wind magnitude in surface layer
    '''
    
    #wind interpolation TBD
    uu = M_10
    
    #effective elevation TBD
    H=H_e

    #source coordinates in rotated crs (X along wind direction)
    (X_r,Y_r)=(config.R @ np.array([X,Y]))

    #distances in meters
    dst_x_r = ((config.wsp_R[:,0]-X_r))
    dst_x_ra = np.abs(dst_x_r)

    #distance only along positive wind direction
    dstx = (dst_x_r+dst_x_ra)/2

    #distance from plume centerline
    dst_y_r = ((config.wsp_R[:,1]-Y_r))    
    
    #dispersion coeficients
    s_y,s_z = calculate_sysz(dstx,stab_class)
    
    #to avoide division by zero
    s_y[s_y == 0]=1e-9
    s_z[s_z == 0]=1e-9

    cc = (Em/(2*np.pi*s_z*s_y*uu)) * np.exp(-0.5*(dst_y_r/s_y)**2) * np.exp(-0.5*(H/s_z)**2)
    
    return cc

def analyze_corr(res,stations, column_sufix ='gauss' ):
    '''
    Analiza korelacji wszystki stacji  - pomiary vs. _gauss lub _ocena
    '''
    coors=dict()
    mse=dict()
    for st in stations:
        try:
            res2 = res[[st + "_measurement", st + '_'+column_sufix]].copy(deep=True)
            res2.dropna(inplace=True)        
            coors[st] = round(res[[st + "_measurement", st + '_'+column_sufix]].corr().iloc[0][1],2)
            #print(res[[st,st+'_measurement',st+'_gauss']].corr())
            mse[st] = mean_squared_error(res2[st + "_measurement"], res2[st + '_'+column_sufix],squared=False)
        except KeyError:
            print(st+'_'+column_sufix+' not found' )
        except ValueError:
            print(st+' error calculating mse!')
    return coors,mse