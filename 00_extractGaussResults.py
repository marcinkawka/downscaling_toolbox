#!/usr/bin/env python3

from utils.config import ScenarioConfig
import argparse
import pandas as pd
from utils.data_loaders import extract_station_ts
from utils.other_tools import lonlat2UTM

'''
Skrypt do wyciągania serii czasowej w punkcie ze stacją(stacjami)
'''

parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
config.create_dirs()

df_stacje = pd.read_csv('csv_input/wszystkie_stacje.csv',sep=';')
if df_stacje.shape[1]==1:
    df_stacje = pd.read_csv('csv_input/wszystkie_stacje.csv',sep=',')
    
df_stacje = df_stacje.set_index(['station_code'])

wybrane_stacje =pd.DataFrame(df_stacje.loc[ config.pm10_stations][['station_lat','station_long']])
wybrane_stacje['x_UTM']=0
wybrane_stacje['y_UTM']=0

for stacja in wybrane_stacje.iterrows():
    st_code = stacja[0]
    wybrane_stacje.loc[st_code,['x_UTM','y_UTM']]=lonlat2UTM(stacja[1]['station_long'],stacja[1]['station_lat'])


res = pd.DataFrame()
for stacja in wybrane_stacje.iterrows():
    x_UTM,y_UTM = lonlat2UTM(stacja[1]['station_long'],stacja[1]['station_lat'])
    
    #df1 = extract_stacja(stacja[1]['x_UTM'],stacja[1]['y_UTM'],start_date,end_date)
    df1 = extract_station_ts(x_UTM,y_UTM,config)

    res[stacja[0]]=df1

res.to_csv(config.csv_output_dir+config.scenario+'_'+\
    config.start_date.strftime('%Y%m%d')+'_'+\
    config.end_date.strftime('%Y%m%d')+'.csv',float_format='%.3f')
