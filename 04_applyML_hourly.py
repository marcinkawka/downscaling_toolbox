#!/usr/bin/env python

import pandas as pd 
import matplotlib.pyplot as plt
from utils.config import ScenarioConfig
import datetime as dt
import argparse
from utils.data_loaders import *
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.tree import export_graphviz
import pydot
import numpy as np
import _pickle as cPickle
import os 

'''
skrypt do stosowania ML
'''
plt.style.use('ggplot')
parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
config.load_ML_config()
os.makedirs(config.tif_hourly_ML_dir,exist_ok=True)

sufix=''
if config.include_dow:
	sufix+='_dow'
else:
	sufix+=''
	
if config.include_meteo_obs:
	sufix+='_meteo'
else:
	sufix+=''


#sufix='_dow_meteo2'
#sufix=''
prefix='rf_model'
#prefix='rgb_model'


with open(config.models_dir+prefix+sufix+'_'+config.scenario+'_'+\
	config.subset_start_date.strftime("%Y%m%d")+'_'+config.subset_end_date.strftime("%Y%m%d")+'.pickle', 'rb') as f:
	rf = cPickle.load(f)


delta = dt.timedelta(hours=1)

if config.include_meteo_obs:
	'''
	Ładowanie danych meteo ze stacji
	'''
	df_meteo = pd.read_csv(config.meteo_observations_file)
	dd =df_meteo[df_meteo['station_name'].isin(config.meteo_stations)]
	del dd['station_name']
	df_tt = dd[dd['paramname']=='TT'].copy(deep=True)
	df_vm = dd[dd['paramname']=='VM'].copy(deep=True)
	df_vd = dd[dd['paramname']=='VD'].copy(deep=True)
	del df_tt['paramname']
	del df_vm['paramname']
	del df_vd['paramname']
	df_tt.set_index(pd.to_datetime(df_tt['to_timestamp']),inplace=True)
	df_vd.set_index(pd.to_datetime(df_vd['to_timestamp']),inplace=True)
	df_vm.set_index(pd.to_datetime(df_vm['to_timestamp']),inplace=True)
	del df_tt['to_timestamp']
	del df_vd['to_timestamp']
	del df_vm['to_timestamp']

hour_ts=[]
is_weekend_ts=[]


#do końca doby włącznie
start_date = config.subset_start_date + dt.timedelta(hours=1)
end_date = config.subset_end_date+ dt.timedelta(hours=23)


while start_date <= config.end_date:
	try:
		if start_date.hour>0:
			date_str = start_date.strftime("%Y%m%d_%-H")
		else:
			date_str = (start_date-dt.timedelta(days=1)).strftime("%Y%m%d_24")
		arr_Pasquill,geoTrans = load_raster(config.tif_hourly_dir+config.scenario+'_'+date_str+'.tif')
		print(date_str)	
	except AttributeError:
		if start_date.hour>0:
			date_str = start_date.strftime("%Y%m%d_%H")
		else:
			date_str = (start_date-dt.timedelta(days=1)).strftime("%Y%m%d_24")
		arr_Pasquill,geoTrans = load_raster(config.tif_hourly_dir+config.scenario+'_'+date_str+'.tif')
		print(date_str)	
	
	date_str = start_date.strftime("%Y%m%d%H%M")
	#arr_GEM,geoTrans = load_raster(config.gem_tif_dir+'/hourly/'+config.scenario+'_'+date_str+'.tif')
	arr_GEM,geoTrans = load_raster(config.gem_tif_dir+config.scenario+'_'+date_str+'.tif')

	#czas	
	hour  = start_date.hour
	month = start_date.month	
	if start_date.weekday() in (5,6):
		is_weekend=1
	else:
		is_weekend=0
	
	

	size = arr_GEM.flatten().shape[0]
	try:
		if config.include_dow:
			if config.include_meteo_obs:
					aa = rf.predict(list(zip(arr_GEM.flatten(),\
									arr_Pasquill.flatten(),\
									hour*np.ones(size),\
									is_weekend*np.ones(size),\
			 						month*np.ones(size),\
									df_tt.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vm.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vd.loc[start_date]['monitoring_value']*np.ones(size)\
									)))
			
			else:
				aa = rf.predict(list(zip(arr_GEM.flatten(),\
									arr_Pasquill.flatten(),\
									hour*np.ones(size),\
									is_weekend*np.ones(size),\
			 						month*np.ones(size)
										)))
		else:
			if config.include_meteo_obs:
				aa = rf.predict(list(zip(arr_GEM.flatten(),\
									arr_Pasquill.flatten(),\
									df_tt.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vm.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vd.loc[start_date]['monitoring_value']*np.ones(size)\
									)))
			else:
				aa = rf.predict(list(zip(arr_Pasquill.flatten(),arr_GEM.flatten())))

		arr_prediction = aa.reshape(arr_Pasquill.shape)
		#arr_prediction = arr_prediction * gem_std + gem_mean

		CreateGeoTiff(config.tif_hourly_ML_dir +config.scenario+'_'+date_str ,arr_prediction,geoTrans,UTM=config.UTM_zone)
		print(start_date)
	except KeyError:
		print('Error processing '+str(start_date))
	
	start_date += delta