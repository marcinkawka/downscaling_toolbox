#!/usr/bin/env python
from asyncio.proactor_events import _ProactorBaseWritePipeTransport
import pandas as pd 
import matplotlib.pyplot as plt
import datetime as dt
from utils.config import ScenarioConfig
import argparse
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import VotingRegressor
from sklearn.model_selection import train_test_split


import numpy as np
import _pickle as cPickle
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score,max_error,mean_squared_error,mean_absolute_error

import os
'''
Jedyny słuszny skrypt do trenowania ML z sklearn
'''

plt.style.use('ggplot')
parser = argparse.ArgumentParser(description='plese give a scenario .ini file')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
config.load_ML_config()

#Histro plotting
df = pd.read_csv(config.csv_output_dir+config.scenario+'_'+\
    config.start_date.strftime("%Y%m%d")+'_'+\
    config.end_date.strftime("%Y%m%d")+'_MLready.csv',index_col=0)
df.index = pd.to_datetime(df.index)
plt.figure(figsize=(30,30))
df.hist(bins=37)
plt.savefig(config.png_diags_dir+'ML_input_hist_'+config.scenario+'.png')
plt.close()

os.makedirs(config.models_dir, exist_ok=True)

def prepare_data(df, stations):
    #prepare uniform data set
    res = pd.DataFrame()
    print("Merging stations ",end='')
    print(stations)
    for col in df.columns:
        if '_measurement' in col:
            st_code = col.split('_')[0]
            if st_code in stations:    
                print(st_code,end=' ')    
                df1 = df[[st_code+'_gauss',st_code+'_gem',st_code+'_measurement']].copy()
                df1.dropna(inplace=True)
                if len(df1)>0:
                    print(len(df1))
                    df1.rename(columns={st_code+'_gauss':'gauss',st_code+'_gem':'gem',st_code+'_measurement':'measurements'},inplace=True)
                    res = pd.concat([res,df1])
                    #print(df1.head())
    return res

def evaluate_model(model,X_test,y_test,model_name=''):
    # Use the forest's predict method on the test data
    predictions = model.predict(X_test)

    # Calculate the absolute errors
    #errors = (abs(predictions - y_test))*res.measurements.std() 
    errors = (abs(predictions - y_test))

    # Print out the mean absolute error (mae)
    mae= round(np.mean(errors), 2)

    # Calculate mean absolute percentage error (MAPE)
    #mape = 100 * (errors / (abs(y_test)*res.measurements.std() +res.measurements.mean()))
    mape = 100 * (errors / (abs(y_test)))

    # Calculate and display accuracy
    accuracy = 100 - np.mean(mape)
    acc = round(accuracy, 2)
    
    #r2 = r2_score( predictions,y_test)
    r2 = r2_score( y_test,predictions)
    me = max_error( predictions,y_test)
    mse = mean_squared_error( predictions,y_test)
    mae2 = mean_absolute_error( predictions,y_test)
    print(model_name+' acc = '+str(acc)+"% mae = "+str(round(mae2,2))+ " me = "+str(round(me,2))+" mse = "+str(round(me,2))+" R2 = "+str(round(r2,3)))
    return mae,acc,r2,me,mae2,mse

# masking the training season
res_train = prepare_data(df,config.train_stations)            
mask = res_train.index.to_series().between(config.subset_start_date, config.subset_end_date)
res_train = res_train[mask].copy()
res_train.dropna(inplace=True)

if len(list(set(config.test_stations).intersection(config.train_stations)))==0:
    res_test = prepare_data(df,config.test_stations)   
    mask_test = res_test.index.to_series().between(config.subset_start_date, config.subset_end_date)
    res_test = res_test[mask_test].copy()
    res_test.dropna(inplace=True)
else:
    #train/test split here
    print('Split training/test data')
    res_train, res_test = train_test_split(res_train, test_size=config.test_set_size)
    
#Shall I include days of the week as additional variable?
if config.include_dow:
    res_train['dow']=res_train.index.weekday
    res_train["hour"] = res_train.index.hour
    res_train["is_weekend"] = res_train["dow"].apply(lambda x: 1 if x in (5, 6) else 0)
    res_train["month"] = res_train.index.month
    del res_train['dow']

    res_test['dow']=res_test.index.weekday
    res_test["hour"] = res_test.index.hour
    res_test["is_weekend"] = res_test["dow"].apply(lambda x: 1 if x in (5, 6) else 0)
    res_test["month"] = res_test.index.month
    del res_test['dow']

#Shall I include meteo station?
if config.include_meteo_obs:
    df_meteo = pd.read_csv(config.meteo_observations_file)
    dd =df_meteo[df_meteo['station_name'].isin(config.meteo_stations)]
    del dd['station_name']
    df_tt = dd[dd['paramname']=='TT'].copy(deep=True)
    df_vm = dd[dd['paramname']=='VM'].copy(deep=True)
    df_vd = dd[dd['paramname']=='VD'].copy(deep=True)
    del df_tt['paramname']
    del df_vm['paramname']
    del df_vd['paramname']
    df_tt.set_index(pd.to_datetime(df_tt['to_timestamp']),inplace=True)
    df_vd.set_index(pd.to_datetime(df_vd['to_timestamp']),inplace=True)
    df_vm.set_index(pd.to_datetime(df_vm['to_timestamp']),inplace=True)
    del df_tt['to_timestamp']
    del df_vd['to_timestamp']
    del df_vm['to_timestamp']
    
    res_train = res_train.join(df_tt)
    res_test = res_test.join(df_tt)
    res_test.rename(columns={'value':'TT'},inplace=True)
    res_train.rename(columns={'value':'TT'},inplace=True)
    res_train = res_train.join(df_vm)
    res_test = res_test.join(df_vm)
    res_test.rename(columns={'value':'VM'},inplace=True)
    res_train.rename(columns={'value':'VM'},inplace=True)
    res_train = res_train.join(df_vd)
    res_test = res_test.join(df_vd)
    res_test.rename(columns={'value':'VD'},inplace=True)
    res_train.rename(columns={'value':'VD'},inplace=True)
    res_train.dropna(inplace=True)
    res_test.dropna(inplace=True)
    
#let's grap the proper column numbers
feature_columns = np.arange(len(res_train.columns)).tolist()
target_column = 2
feature_columns.pop(target_column)

aa_train = res_train.to_numpy()
aa_test = res_test.to_numpy()

print('Features are'+str(res_train.columns[feature_columns]))
print('The target is set to '+res_train.columns[target_column])
X_train = aa_train[:,feature_columns]
X_test = aa_test[:,feature_columns]
y_train = aa_train[:,target_column]
y_test = aa_test[:,target_column]
feature_list = res_train.columns[feature_columns]

print('Training Features Shape:', X_train.shape)
print('Training Labels Shape:', y_train.shape)
print('Testing Features Shape:', X_test.shape)
print('Testing Labels Shape:', y_test.shape)


'''
Creating ML-model
'''
# prepare cross validation
FOLDS=5
ACC=0
R2=0
kfold = KFold(n_splits=FOLDS, shuffle=True, random_state=2)
print('include dow : '+str(config.include_dow)+' include meteo: '+str(config.include_meteo_obs))
print('Ready for fitting!')
print('Fitting RandomForest...')

for i,(train, test) in enumerate(kfold.split(X_train,y_train)):
    rf = RandomForestRegressor(n_estimators = 513,max_depth=5, random_state=2,criterion="absolute_error",n_jobs=48)
    rf.fit(X_train[train],y_train[train])
    print(f"\tFold = {i} ",end='')
    mae,acc,r2,me,mae2,mse = evaluate_model(rf,X_train[test],y_train[test],model_name='RF_kfold ')
    ACC+=acc
    R2+=r2
    
print('DONE!')

print('___________RESULTS________________')
print('include dow : '+str(config.include_dow)+' include meteo: '+str(config.include_meteo_obs))
print(f"KFLOD results Acc = {round(ACC/FOLDS,2)}%, R2 ={round(R2/FOLDS,2)}")
evaluate_model(rf,X_test,y_test,model_name='test set evaluation: ')

def create_model_filename(model_name, config):
    prefix = model_name
    if config.include_dow:
        prefix = prefix +'_dow'
    if config.include_meteo_obs:
        prefix = prefix +'_meteo'
    prefix = prefix+'_'+config.scenario+'_'+\
        config.subset_start_date.strftime("%Y%m%d")+'_'+\
        config.subset_end_date.strftime("%Y%m%d")
    return prefix

    
filename = create_model_filename('rf_model', config)
with open(config.models_dir+filename+'.pickle', 'wb') as f:
    cPickle.dump(rf, f)

