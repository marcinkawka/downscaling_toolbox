
from data_loaders import *
import numpy as np
import scipy
from scipy import ndimage, signal
from skimage.measure import block_reduce

global geoTrans
arr, geoTrans   = load_raster('shp/drogi_krajowe.tif')
'''
a=arr
dc=a[:,range(0,a.shape[1],2)]
drdc=dc[range(0,a.shape[0],2),:]
print(a.shape)
print(drdc.shape)
'''
'''
def block_mean(ar, fact):
    assert isinstance(fact, int), type(fact)
    sx, sy = ar.shape
    X, Y = np.ogrid[0:sx, 0:sy]
    regions = round(sy/fact) * round((X/fact) + round(Y/fact)
    res = ndimage.mean(ar, labels=regions, index=np.arange(regions.max() + 1))
    res.shape = (round(sx/fact), round(sy/fact))
    return res
'''
#CreateGeoTiff("tif_output/wynik_resampling.tif", arr)
arr2 = block_reduce(arr, block_size=(10, 10), func=np.max)
gt2=list(geoTrans)
gt2[1]=100.0
gt2[5]=-100.0
geoTrans=tuple(gt2)
CreateGeoTiff("tif_output/wynik_resampling_drdc.tif", arr2,geoTrans)