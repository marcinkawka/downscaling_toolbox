#!/usr/bin/env python
from rpn_tools import *
import pprint as pp
import matplotlib.pyplot as plt
import pandas as pd
import sys
import datetime as dt
import argparse
from rpn.rpn_multi import MultiRPN

'''
Skrypt do wyciągania serii czasowej w danym punkcie z rpnów MIESIECVZNYCH z oceną
Lista stacji ze współrzędnymi pochodzi z pliku csv.
'''

#dla nearest jest znacznie szybciej ale mniej "fizycznie"
interp_method = 'linear'
RD = 287.05 #[J/KG/K]
RM_PM10 = 28.9

lista_stacji = 'csv_input/wszystkie_stacje.csv'
rpn_dir ='/mnt/disk2/mkawka/ocena2021/px/'

parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
args = parser.parse_args()

start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))
delta = dt.timedelta(days=1)

df_lista_stacji = pd.read_csv(lista_stacji,sep=';')
df_lista_stacji.set_index(['id_station'],inplace=True)

station_codes =  df_lista_stacji['station_code'].unique()

#jeśli chcemy ograniczyć liczbę przetwarzanych stacyji
#station_codes = ['SlBielKossak','DsKlodzSzkol','SlZywieKoper','DsJelGorOgin','PkRymZdrPark','PmLebaRabkaE',\
#'DsLadekMOB','DsDzialoszyn','MpKrynicDiet','MpKrynicDiet','PkKrempnaMPN','PkJasloSikor']
#SPEC = SPEC *  P0 * 100./(RD*(TT+273.15)) * RM/28.9 * 1.E+9   ! CONVERT TO UG/M3

res =pd.DataFrame()
while start_date <= end_date:
    print(start_date.year)	
    rpn_file = rpn_dir+'control_diag_'+str(start_date.year)+'_18_PX10_month_'+str(start_date.month).zfill(2)+'.rpn'	
    r =RPN(rpn_file)
    pm10 = r.get_4d_field('PX10')
    suma_dobowa = None
    licznik = 0

    #pętla po czasie miesiaca
    for key, value in pm10.items():
        src_array_pm10 = value[1.0]
        if (key>=start_date and key<=end_date):
            try:
                grid_x,grid_y,points = rpn2grid(rpn_file,varname='PX10',dlat=0.025,dlon=0.025)
                array = np.flipud(rpn_interpolate(points,src_array_pm10.flatten(),grid_x,grid_y,method=interp_method))
            

                for stc in station_codes:
                    try:
                        st_lat = df_lista_stacji[df_lista_stacji.station_code==stc]['station_lat'].values[0]
                        st_lon = df_lista_stacji[df_lista_stacji.station_code==stc]['station_long'].values[0]
                        (xx,yy) = find_nearest_idx2d2d(grid_x,grid_y, st_lon, st_lat)
                        res.at[key,stc]=array[yy,xx]
                    except:
                        print("Nie udało się pobrać danych dla "+str(stc))
                start_date += dt.timedelta(hours=1)

                print(key.strftime("%Y%m%d %H:%M"))
            except:
                print("Nie udało się pobrać danych dla "+str(stc))
            
    r.close()
   

    
    print(start_date.strftime("%Y%m%d"))    
    res.to_csv('csv_input/px_ocena_ts'+str(args.d0)+'.csv')
