#!/usr/bin/env python
import pandas as pd 
import datetime as dt
import argparse
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import os
import sys

'''
Skrypt do analizy godzinowych wyników RF
(przydatny do porówynywania strategii uczenia)
Zgodnie ze sztuką powinien być zastosowany do zbioru pomiarów na którym
nie było przeprowadzane uczenie (niezależna validacja)
'''

pd.options.display.float_format = '{:,.2f}'.format
plt.style.use('ggplot')

#mnoznik dla Gaussa:
mnoznik=1
parser = argparse.ArgumentParser(description='')
parser.add_argument('scenario', metavar='scenario', type=str,help="nazwa scenariusza")
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
args = parser.parse_args()
scenario = args.scenario

start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))

stacje = {'Klodzko':'DsKlodzSzkol','Bielski':'SlBielKossak',\
    'Bieszczadzki':'PkPolanZdrojMOB','Nowosadecki':'MpNoSaczNadb',\
    'Nowotarski':'MpNoTargPSlo','Zgorzelecki':'DsLubanMieszMOB',
    'Cieszynski':'SlCiesChopin','Zywiecki':'SlZywieKoper',\
    'Myslenicki':'MpMyslenSoli','Tatrzanski':'MpZakopaSien',\
    'Gorlicki':'MpSzymbaGorl','Krosnienski':'PkKrosKletow',\
    'Wadowicki':'MpSuchaNiesz',\
    'Walbrzyski':'DsWalbrzWyso','Kamiennogorski':'DsKamGoraMOB',\
    'Karkonoski':'DsJelGorOgin','Zabkowicki':'DsDziePilsud',\
    'Nyski':'OpNysaRodzie','Glubczycki':'OpGlubRatusz',
    'Poludniowoslaskie':['SlBielKossak','SlCiesChopin','SlCiesMickie','SlUstronSana',\
        'SlZywieKoper','SlWodzGalczy','SlRybniBorki','SlCzerKopaln',\
        'MpSuchaNiesz','SlBielPartyz','SlZorySikor2','MpOswiecBema',\
        'SlGoczaUzdroMOB'],\
    'Krakowskie':['MpKrakAlKras','MpKrakBulwar','MpKrakOsPias','MpKrakWadow',\
    'MpKrakZloRog','MpKrakDietla','MpKrakBujaka','MpKaszowLisz','MpZabieWapie','MpNiepo3Maja']
}

stacja = stacje[scenario]
sufix = ''

res= pd.read_csv('csv_output/'+scenario+'_'+\
    start_date.strftime('%Y%m%d')+'_'+end_date.strftime('%Y%m%d')+\
        sufix+'_caloscHourly.csv')
res.set_index(pd.to_datetime(res['Unnamed: 0']),inplace=True)
del res['Unnamed: 0']

res_avg24= res.groupby(pd.Grouper(freq="1D")).mean()
res_max24= res.groupby(pd.Grouper(freq="1D")).max()

time_period = (res.index[-1] - res.index[0]) /4


print('początek: '+str(res.index[0]))
print('koniec: '+str(res.index[-1]))
print('okres 1/4: '+str(time_period))
for st in stacja:
    start = res.index[0]
    end = res.index[0]+time_period
    try:
        print(st)
        res_1st = res[[st+'_measurement',st+'_gauss',st+'_ocena',st+'_RF']]
        res_1st_avg = res_avg24[[st+'_measurement',st+'_gauss',st+'_ocena',st+'_RF']]
        res_1st_max = res_max24[[st+'_measurement',st+'_gauss',st+'_ocena',st+'_RF']]

        fig, axes = plt.subplots(nrows=4, ncols=1,figsize=(20,10))
        fig.subplots_adjust(hspace=.5)

        res_1st.loc[start:end,st+'_ocena'].plot(ax=axes[0],lw=0.5)
        res_1st.loc[start:end,st+'_measurement'].plot(ax=axes[0],lw=0.5)
        (res_1st.loc[start:end,st+'_gauss']*mnoznik).plot(ax=axes[0],lw=0.5)
        res_1st.loc[start:end,st+'_RF'].plot(ax=axes[0],lw=0.5)
        
        start = res.index[0]+time_period
        end = res.index[0]+time_period*2

        res_1st.loc[start:end,st+'_ocena'].plot(ax=axes[1],lw=0.5)
        res_1st.loc[start:end,st+'_measurement'].plot(ax=axes[1],lw=0.5)
        (res_1st.loc[start:end,st+'_gauss']*mnoznik).plot(ax=axes[1],lw=0.5)
        res_1st.loc[start:end,st+'_RF'].plot(ax=axes[1],lw=0.5)
        
        start = res.index[0]+time_period*2
        end = res.index[0]+time_period*3

        res_1st.loc[start:end,st+'_ocena'].plot(ax=axes[2],lw=0.5)
        res_1st.loc[start:end,st+'_measurement'].plot(ax=axes[2],lw=0.5)
        (res_1st.loc[start:end,st+'_gauss']*mnoznik).plot(ax=axes[2],lw=0.5)
        res_1st.loc[start:end,st+'_RF'].plot(ax=axes[2],lw=0.5)
        
        start = res.index[0]+time_period*3
        end = res.index[0]+time_period*4

        res_1st.loc[start:end,st+'_ocena'].plot(ax=axes[3],lw=0.5)
        res_1st.loc[start:end,st+'_measurement'].plot(ax=axes[3],lw=0.5)
        (res_1st.loc[start:end,st+'_gauss']*mnoznik).plot(ax=axes[3],lw=0.5)
        res_1st.loc[start:end,st+'_RF'].plot(ax=axes[3],lw=0.5)
        
        axes[0].set_title(st+' hourly ts')
        axes[0].legend([st+'_gem-aq',st+'_measurement',st+'_gauss',st+'_RF'])
        
        plt.savefig("png/"+scenario+'/TS1h/'+st+sufix+'.png',dpi=300)
        plt.close()


        fig, axes = plt.subplots(nrows=1, ncols=1,figsize=(20,10))
        res_1st_avg[st+'_ocena'].plot(ax=axes,lw=0.5)
        res_1st_avg[st+'_measurement'].plot(ax=axes,lw=0.5)
        (res_1st_avg[st+'_gauss']*mnoznik).plot(ax=axes,lw=0.5)
        res_1st_avg[st+'_RF'].plot(ax=axes,lw=0.5)

        axes.set_title(st+' daily avg ts')
        plt.legend([st+'_gem-aq',st+'_measurement',st+'_gauss',st+'_RF'])
        plt.savefig("png/"+scenario+'/TS24h_avg/'+st+sufix+'.png',dpi=300)
        plt.close()

        fig, axes = plt.subplots(nrows=1, ncols=1,figsize=(20,10))
        res_1st_max[st+'_ocena'].plot(ax=axes,lw=0.5)
        res_1st_max[st+'_measurement'].plot(ax=axes,lw=0.5)
        (res_1st_max[st+'_gauss']*mnoznik).plot(ax=axes,lw=0.5)
        res_1st_max[st+'_RF'].plot(ax=axes,lw=0.5)

        axes.set_title(st+' daily max ts')
        plt.legend([st+'_gem-aq',st+'_measurement',st+'_gauss',st+'_RF'])
        plt.savefig("png/"+scenario+'/TS24h_max/'+st+sufix+'.png',dpi=300)
        plt.close()
        
    except KeyError:
        print(st+' not found!')