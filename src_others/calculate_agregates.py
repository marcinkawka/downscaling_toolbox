#!/usr/bin/env python

from data_loaders import *
import matplotlib.pyplot as plt
import datetime as dt
import argparse
from data_loaders import *
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.tree import export_graphviz
import pydot
import numpy as np
import _pickle as cPickle
import os 

rf_folder = 'tif_output/RF_hourly/'
prognoza_folder = 'tif_output/ocena_hourly_px/'
gauss_folder = 'tif_output/hourly/'
output_folder='tif_final/'

parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('scenario', metavar='scenario', type=str,help="nazwa scenariusza")
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
args = parser.parse_args()
scenario = args.scenario

start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))
delta = dt.timedelta(hours=1)


res_gem = []
res_rf = []
res_gauss =[]

daily_gem = []
daily_rf = []
daily_gauss =[]

hour = 1
while start_date <= end_date:
	date_str = start_date.strftime("%Y%m%d%H%M")
	
	try:
		arr_rf,geoTrans = load_raster(rf_folder +scenario+'_'+date_str+'.tif')
		res_rf.append(arr_rf)
	except AttributeError:
		print('Failed to open '+rf_folder +scenario+'_'+date_str+'.tif')
	
	try:
		arr_gem,geoTrans = load_raster(prognoza_folder +scenario+'/'+scenario+'_'+date_str+'.tif')
		res_gem.append(arr_gem)
	except AttributeError:
		print('Failed to open '+prognoza_folder+scenario+'/' +scenario+'_'+date_str+'00.tif')
	

	date_str = start_date.strftime("%Y%m%d_")
	try:
		arr_gauss,geoTrans = load_raster(gauss_folder +scenario+'/'+scenario+'_'+date_str+str(hour)+'.tif')
		res_gauss.append(arr_gauss)
		
	except AttributeError:
		print('Failed to open '+gauss_folder+scenario+'/' +scenario+'_'+date_str+'00.tif')
	

	start_date += delta
	hour+=1
	if hour==25:
		print(start_date)
		hour=1
		daily_gem.append( np.mean(np.array(res_gem[:-24]),axis=0))
		daily_gauss.append( np.mean(np.array(res_gauss[:-24]),axis=0))
		daily_rf.append( np.mean(np.array(res_rf[:-24]),axis=0))

all_res_gem = np.array(res_gem)
all_res_gem_daily = np.array(daily_gem)

all_res_rf = np.array(res_rf)

all_res_gauss = np.array(res_gauss)
all_res_gauss_daily = np.array(daily_gauss)

all_res_rf_daily = np.array(daily_rf)

print('Calculating statistics... this may take a while')
res_mean_gem = all_res_gem.mean(axis=0)
res_mean_rf = all_res_rf.mean(axis=0)
res_mean_gauss = all_res_gauss.mean(axis=0)

res_p902_gem = np.percentile(all_res_gem,90.2,axis=0)
res_p902_rf = np.percentile(all_res_rf,90.2,axis=0)
res_p902_gauss = np.percentile(all_res_gauss,90.2,axis=0)

res_d50_gem = np.sum(all_res_gem_daily>50,axis=0)
res_d50_gauss = np.sum(all_res_gauss_daily>50,axis=0)
res_d50_rf = np.sum(all_res_rf_daily>50.0,axis=0)


CreateGeoTiff(output_folder+scenario+'_mean_rf' ,res_mean_rf,geoTrans)
CreateGeoTiff(output_folder+scenario+'_mean_gem' ,res_mean_gem,geoTrans)
CreateGeoTiff(output_folder+scenario+'_mean_gauss' ,res_mean_gauss,geoTrans)
CreateGeoTiff(output_folder+scenario+'_d50_gem' ,res_d50_gem,geoTrans)
CreateGeoTiff(output_folder+scenario+'_d50_gauss' ,res_d50_gauss,geoTrans)
CreateGeoTiff(output_folder+scenario+'_d50_rf' ,res_d50_rf,geoTrans)
CreateGeoTiff(output_folder+scenario+'_p902_gem' ,res_p902_gem,geoTrans)
CreateGeoTiff(output_folder+scenario+'_p902_gauss' ,res_p902_gauss,geoTrans)
CreateGeoTiff(output_folder+scenario+'_p902_rf' ,res_p902_rf,geoTrans)