#!/usr/bin/env python
from rpn_tools import *
import pprint as pp
import matplotlib.pyplot as plt
import pandas as pd
import sys
import datetime as dt
import argparse
from rpn.rpn_multi import MultiRPN
from config import ScenarioConfig

'''
Skrypt do wyciągania serii czasowej w danym punkcie z rpnów GODZINOWYCH z oceną
Lista stacji ze współrzędnymi pochodzi z pliku csv.
'''
RD = 287.05 #[J/KG/K]
RM_PM10 = 28.9

lista_stacji = 'csv_input/wszystkie_stacje.csv'


parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('scenario_file', metavar='scenario_file', type=str,help="scenario file")
args = parser.parse_args()

config = ScenarioConfig(args.scenario_file)
config.load_config()
config.load_ML_config()

rpn_dir = config.rpn_dir
start_date = config.subset_start_date
end_date = config.subset_end_date
delta = dt.timedelta(days=1)

df_lista_stacji = pd.read_csv(lista_stacji,sep=';')
if len(df_lista_stacji.columns)==1:
    df_lista_stacji = pd.read_csv(lista_stacji,sep=',')
df_lista_stacji.set_index(['id_station'],inplace=True)
    

station_codes =  df_lista_stacji['station_code'].unique()
#station_codes = ['SlBielKossak','DsKlodzSzkol','SlZywieKoper','DsJelGorOgin','PkRymZdrPark','PmLebaRabkaE',\
#'DsLadekMOB','DsDzialoszyn','MpKrynicDiet','MpKrynicDiet','PkKrempnaMPN','PkJasloSikor']
#SPEC = SPEC *  P0 * 100./(RD*(TT+273.15)) * RM/28.9 * 1.E+9   ! CONVERT TO UG/M3
start_date0=start_date
res =pd.DataFrame()
while start_date <= end_date:
    date = start_date.strftime("%Y%m%d")

    date_str = start_date.strftime("%Y%m%d")
    path_list_km =[rpn_dir+date_str+'00/km'+date_str+'00-00-00_000000'+str(i).zfill(2)+'h' for i in range(1,25)]
    path_list_dm =[rpn_dir+date_str+'00/dm'+date_str+'00-00-00_000000'+str(i).zfill(2)+'h' for i in range(1,25)]

    r_km = MultiRPN(path_list_km)
    r_dm = MultiRPN(path_list_dm)

    pt25 = r_km.get_4d_field(varname='PT25')
    ptco = r_km.get_4d_field(varname='PTCO')
    ph25 = r_km.get_4d_field(varname='PH25')
    phco = r_km.get_4d_field(varname='PHCO')
    
    tt   = r_dm.get_4d_field(varname='TT')
    p0   = r_dm.get_4d_field(varname='P0')

    #pętla po czasie dnia
    for key, value in pt25.items():
        src_array_pm10 = 3*value[1.0]+2*ptco[key][1.0]+ph25[key][1.0]+phco[key][1.0]
        P0 = p0[key][0.0]
        TT = tt[key][1.0]

        rho_air = (P0*100)/(RD*(TT+273.15))
        
        src_array_pm10 = src_array_pm10 *  P0 * 100./(RD*(TT+273.15))  * 1.E+9 +3

        try:
            grid_x,grid_y,points = rpn2grid(path_list_km[0],varname='PT25',dlat=0.025,dlon=0.025)
            array = np.flipud(rpn_interpolate(points,src_array_pm10.flatten(),grid_x,grid_y,method='linear'))
            for stc in station_codes:
                try:
                    st_lat = df_lista_stacji[df_lista_stacji.station_code==stc]['station_lat'].values[0]
                    st_lon = df_lista_stacji[df_lista_stacji.station_code==stc]['station_long'].values[0]
                    (xx,yy) = find_nearest_idx2d2d(grid_x,grid_y, st_lon, st_lat)
                    res.at[key,stc]=array[yy,xx]
                except:
                    print("Nie udało się pobrać danych dla "+str(stc))
            
            print(key.strftime("%Y%m%d %H:%M"))
            
        except Exception as e:
            print(e)
            
    r_km.close()
    r_dm.close()

    start_date += delta
    print(res.head())
    print(start_date.strftime("%Y%m%d"))    
    res.to_csv(config.csv_output_dir+'/ocena_ts'+start_date0.strftime("%Y%m%d")+'.csv')
