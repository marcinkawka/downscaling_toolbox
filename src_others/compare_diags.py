from data_loaders import *
from other_tools import *
import pandas as pd 

stacje = {'Klodzko':'DsKlodzSzkol','Bielski':'SlBielKossak',\
    'Bieszczadzki':'PkPolanZdrojMOB','Nowosadecki':'MpNoSaczNadb',\
    'Nowotarski':'MpNoTargPSlo','Zgorzelecki':'DsLubanMieszMOB',
    'Cieszynski':'SlCiesChopin','Zywiecki':'SlZywieKoper',\
    'Myslenicki':'MpMyslenSoli','Tatrzanski':'MpZakopaSien',\
    'Gorlicki':'MpSzymbaGorl','Krosnienski':'PkKrosKletow',\
    'Wadowicki':'MpSuchaNiesz',\
    'Walbrzyski':'DsWalbrzWyso','Kamiennogorski':'DsKamGoraMOB',\
    'Karkonoski':'DsJelGorOgin','Zabkowicki':'DsDziePilsud',\
    'Nyski':'OpNysaRodzie','Glubczycki':'OpGlubRatusz'
}
lista_stacji = 'csv_input/wszystkie_stacje.csv'
df_lista_stacji = pd.read_csv(lista_stacji,sep=';')
df_lista_stacji.set_index(['station_code'],inplace=True)

df_gios = pd.read_excel('diag/statystyki_roczne_PM10.xls',sheet_name='GIS')
df_gios.set_index(['name'],inplace=True)
diag = 'AVYR'

wynik = pd.DataFrame(columns=[diag+'_gios',diag+'_rf',diag+'_gem'])



arr,gt = load_raster('tif_masked/PX_POLACZONE_W_mean_rf.tif')
arr_gem,gt = load_raster('tif_masked/PX_POLACZONE_W_mean_gem.tif')

for _, stc in stacje.items():
    print(stc)
    lat = df_lista_stacji.loc[stc]["station_lat"]
    lon = df_lista_stacji.loc[stc]["station_long"]
    x,y = lonlat2UTM(lon,lat,UTM=33)
    (pixel, line) = world2Pixel(gt, x, y)
    try:
        if stc[:2] in ['Ds','Op']:                
            print(stc)               
            wynik.loc[stc, [diag+"_gios",diag+"_gem",diag+"_rf"]] =\
                [df_gios.loc[stc][diag],arr_gem[line][pixel],arr[line][pixel]]
    except Exception:
        pass


arr,gt = load_raster('tif_masked/PX10_POLACZONE_E_mean_rf.tif')
arr_gem,gt = load_raster('tif_masked/PX10_POLACZONE_E_mean_gem.tif')


for _, stc in stacje.items():
    print(stc)
    lat = df_lista_stacji.loc[stc]["station_lat"]
    lon = df_lista_stacji.loc[stc]["station_long"]
    x,y = lonlat2UTM(lon,lat,UTM=34)
    (pixel, line) = world2Pixel(gt, x, y)
    try:
        if stc[:2] in ['Sl','Mp','Pk']:                
            print(stc)  
            wynik.loc[stc, [diag+"_gios",diag+"_gem",diag+"_rf"]] =\
                [df_gios.loc[stc][diag],arr_gem[line][pixel],arr[line][pixel]]
    except Exception as e:
        print(e)
        pass

diag = 'P904'

arr,gt = load_raster('tif_masked/PX_POLACZONE_W_p904_rf.tif')
arr_gem,gt = load_raster('tif_masked/PX_POLACZONE_W_p904_gem.tif')

for _, stc in stacje.items():
    print(stc)
    lat = df_lista_stacji.loc[stc]["station_lat"]
    lon = df_lista_stacji.loc[stc]["station_long"]
    x,y = lonlat2UTM(lon,lat,UTM=33)
    (pixel, line) = world2Pixel(gt, x, y)
    try:
        if stc[:2] in ['Ds','Op']:                
            print(stc)
            wynik.loc[stc, [diag+"_gios",diag+"_gem",diag+"_rf"]] =\
                [df_gios.loc[stc][diag],arr_gem[line][pixel],arr[line][pixel]]
    except Exception:
        pass


arr,gt = load_raster('tif_masked/PX10_POLACZONE_E_p904_rf.tif')
arr_gem,gt = load_raster('tif_masked/PX10_POLACZONE_E_p904_gem.tif')


for _, stc in stacje.items():
    print(stc)
    lat = df_lista_stacji.loc[stc]["station_lat"]
    lon = df_lista_stacji.loc[stc]["station_long"]
    x,y = lonlat2UTM(lon,lat,UTM=34)
    (pixel, line) = world2Pixel(gt, x, y)
    try:
        if stc[:2] in ['Sl','Mp','Pk']:                
            print(stc)  
            wynik.loc[stc, [diag+"_gios",diag+"_gem",diag+"_rf"]] =\
                [df_gios.loc[stc][diag],arr_gem[line][pixel],arr[line][pixel]]
    except Exception as e:
        print(e)
        pass

diag = 'D050'

arr,gt = load_raster('tif_masked/PX_POLACZONE_W_d50_rf.tif')
arr_gem,gt = load_raster('tif_masked/PX_POLACZONE_W_d50_gem.tif')

for _, stc in stacje.items():
    
    lat = df_lista_stacji.loc[stc]["station_lat"]
    lon = df_lista_stacji.loc[stc]["station_long"]
    x,y = lonlat2UTM(lon,lat,UTM=33)
    (pixel, line) = world2Pixel(gt, x, y)
    try:
        if stc[:2] in ['Ds','Op']:                
            print(stc)
            wynik.loc[stc, [diag+"_gios",diag+"_gem",diag+"_rf"]] =\
                [df_gios.loc[stc][diag],arr_gem[line][pixel],arr[line][pixel]]
    except Exception:
        pass


arr,gt = load_raster('tif_masked/PX10_POLACZONE_E_d50_rf.tif')
arr_gem,gt = load_raster('tif_masked/PX10_POLACZONE_E_d50_gem.tif')


for _, stc in stacje.items():
    print(stc)
    lat = df_lista_stacji.loc[stc]["station_lat"]
    lon = df_lista_stacji.loc[stc]["station_long"]
    x,y = lonlat2UTM(lon,lat,UTM=34)
    (pixel, line) = world2Pixel(gt, x, y)
    try:
        if stc[:2] in ['Sl','Mp','Pk']:                
            print(stc)
            wynik.loc[stc, [diag+"_gios",diag+"_gem",diag+"_rf"]] =\
                [df_gios.loc[stc][diag],arr_gem[line][pixel],arr[line][pixel]]
    except Exception as e:
        print(e)
        pass

wynik['SD_AVYR_gem'] = (wynik['AVYR_gios'] -wynik['AVYR_gem'])**2
wynik['SD_AVYR_rf'] = (wynik['AVYR_gios'] -wynik['AVYR_rf'])**2

wynik['SD_P904_gem'] = (wynik['P904_gios'] -wynik['P904_gem'])**2
wynik['SD_P904_rf'] = (wynik['P904_gios'] -wynik['P904_rf'])**2

wynik['SD_D050_gem'] = (wynik['D050_gios'] -wynik['D050_gem'])**2
wynik['SD_D050_rf'] = (wynik['D050_gios'] -wynik['D050_rf'])**2

wynik.to_csv('diag/calosc.csv')