#!/usr/bin/env python

import pandas as pd 
import matplotlib.pyplot as plt
import datetime as dt
import argparse
from data_loaders import *
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.tree import export_graphviz
import pydot
import numpy as np
import _pickle as cPickle
import os 

'''
zastosowanei modelu ML, potrzebne wyniki Pasquilla oraz prognoza w tifach
'''
parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('scenario', metavar='scenario', type=str,help="nazwa scenariusza")
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
parser.add_argument('--include_dow', dest='include_dow', action='store_true',help="shall I include days of the week?")
parser.add_argument('--no-include_dow', dest='include_dow', action='store_false')
parser.add_argument('--station_meteo', dest='station_meteo', action='store_true',help="shall I include days of the week?")
parser.add_argument('--no-station_meteo', dest='station_meteo', action='store_false')
parser.set_defaults(feature=False)
args = parser.parse_args()

scenario = args.scenario
start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))
include_dow=bool(args.include_dow)
station_meteo=bool(args.station_meteo)

start_year=dt.datetime(2021,1,1,0,0)
end_year=dt.datetime(2021,12,31,0,0)

if station_meteo:
	sufix='_meteo2'
else:
	sufix=''
if include_dow:
    sufix+='_dow'
else:
    sufix+=''

#sufix='_dow_meteo2'
#sufix=''
#prefix='rf_model'
prefix='rgb_model'
output_folder = 'tif_output/'+prefix+'_'+sufix+'/'
date_str='20210101'
try:
	os.mkdir(output_folder)
except FileExistsError:
	pass

with open('models/'+prefix+'_'+scenario+'_'+start_year.strftime("%Y%m%d")+'_'+end_year.strftime("%Y%m%d")+sufix+'.pickle', 'rb') as f:
	rf = cPickle.load(f)


#zbędne
#df = pd.read_csv('csv_output/'+scenario+'_'+start_date.strftime("%Y%m%d")+'_MLready.csv',index_col=0)
#df.index = pd.to_datetime(df.index)
#df = df.groupby(pd.Grouper(freq='1D')).mean()

delta = dt.timedelta(hours=1)

if scenario in ['Klodzko','Zgorzelecki','Karkonoski','Zabkowicki',\
	'Walbrzyski','Glubczycki','Nyski','Kamiennogorski']:
	UTM=33
else:
	UTM=34

'''
Ładowanie danych meteo ze stacji
'''

df_tt = pd.read_csv('csv_input/tt_bielsko2021.csv')
df_tt.set_index(pd.to_datetime(df_tt['czas']),inplace=True)
df_vm = pd.read_csv('csv_input/vm_bielsko2021.csv')
df_vm.set_index(pd.to_datetime(df_vm['czas']),inplace=True)
df_vd = pd.read_csv('csv_input/vd_bielsko2021.csv')
df_vd.set_index(pd.to_datetime(df_vd['czas']),inplace=True)

'''
Ładowanie danych meteo
'''
#input_meteo_file = 'csv_output/'+scenario+'_'+start_year.strftime("%Y%m%d")+'_'+end_year.strftime("%Y%m%d")+'_meteo.csv'
#df_meteo = pd.read_csv(input_meteo_file)
#df_meteo.set_index(pd.to_datetime(df_meteo['Unnamed: 0']),inplace=True)
#del df_meteo['Unnamed: 0']

hour_ts=[]
is_weekend_ts=[]

dTdz28=[]
temp28=[]
vm=[]
vd=[]


start_date += delta
while start_date <= end_date:
	if start_date.hour>0:
		date_str = start_date.strftime("%Y%m%d_%-H")
	else:
		date_str = (start_date-dt.timedelta(days=1)).strftime("%Y%m%d_24")
	
	print(date_str)	
	
	arr_Pasquill,geoTrans = load_raster('tif_output/hourly/'+scenario+'/'+scenario+'_'+date_str+'.tif')
	date_str = start_date.strftime("%Y%m%d%H%M")
	arr_GEM,geoTrans = load_raster('tif_output/ocena_hourly_px/'+scenario+'/'+scenario+'_'+date_str+'.tif')

	#czas	
	hour = start_date.hour
	if start_date.weekday() in (5,6):
		is_weekend=1
	else:
		is_weekend=0
	#meteo
	#dTdz28 = df_meteo.loc[start_date]['dTdz28']
	#temp28 = df_meteo.loc[start_date]['temp28']
	#vm = df_meteo.loc[start_date]['vm']
	#vd = df_meteo.loc[start_date]['vd']
	
	#
	#	STANDARYZACJA
	#
	#arr_GEM = (arr_GEM - df.ocena.mean())/df.ocena.std()
	#arr_Pasquill = (arr_Pasquill - df.Pasquill_value.mean())/df.Pasquill_value.std()
	#gem_mean = np.mean(arr_GEM)
	#gem_std =  np.std(arr_GEM)
	#pasquill_mean = np.mean(arr_Pasquill)
	#pasquill_std =  np.std(arr_Pasquill)
	
	#arr_GEM = (arr_GEM - gem_mean)/gem_std
	#arr_Pasquill = (arr_Pasquill - pasquill_mean)/pasquill_std
	size = arr_GEM.flatten().shape[0]
	try:
		if include_dow:
			if station_meteo:
					aa = rf.predict(list(zip(arr_GEM.flatten(),\
									arr_Pasquill.flatten(),\
									hour*np.ones(size),\
									is_weekend*np.ones(size),\
									df_tt.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vm.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vd.loc[start_date]['monitoring_value']*np.ones(size)\
									)))
			
			else:
				aa = rf.predict(list(zip(arr_GEM.flatten(),\
									arr_Pasquill.flatten(),\
									hour*np.ones(size),\
									is_weekend*np.ones(size),\
									#dTdz28*np.ones(size),\
									#temp28*np.ones(size),\
									#vm*np.ones(size),\
									#vd*np.ones(size)\
										)))
		else:
			if station_meteo:
				aa = rf.predict(list(zip(arr_GEM.flatten(),\
									arr_Pasquill.flatten(),\
									df_tt.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vm.loc[start_date]['monitoring_value']*np.ones(size),\
									df_vd.loc[start_date]['monitoring_value']*np.ones(size)\
									)))
			else:
				aa = rf.predict(list(zip(arr_Pasquill.flatten(),arr_GEM.flatten())))

		arr_prediction = aa.reshape(arr_Pasquill.shape)
		#arr_prediction = arr_prediction * gem_std + gem_mean

		CreateGeoTiff(output_folder+scenario+'_'+date_str ,arr_prediction,geoTrans,UTM=UTM)
		print(start_date)
	except KeyError:
		print('Error processing '+str(start_date))
	
	start_date += delta