#!/usr/bin/env python

import pandas as pd 
import matplotlib.pyplot as plt
import datetime as dt
import argparse
from data_loaders import *
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.tree import export_graphviz
import pydot
import numpy as np
import _pickle as cPickle
import os 

'''
zastosowanei modelu ML, potrzebne wyniki Pasquilla oraz prognoza w tifach
'''
parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
args = parser.parse_args()

start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))

output_folder = 'tif_output/RF_daily/'
date_str='20210101'
try:
	os.mkdir(output_folder)
except FileExistsError:
	pass


with open('rf_model.pickle', 'rb') as f:
	rf = cPickle.load(f)

df = pd.read_csv('csv_output/Klodzko_'+start_date.strftime("%Y%m%d")+'_MLready.csv',index_col=0)
df.index = pd.to_datetime(df.index)
df = df.groupby(pd.Grouper(freq='1D')).mean()

delta = dt.timedelta(days=1)

while start_date <= end_date:
	date_str = start_date.strftime("%Y%m%d")
		
	arr_Pasquill,geoTrans = load_raster('tif_output/daily/Klodzko_'+date_str+'.tif')
	arr_GEM,geoTrans = load_raster('tif_output/prognoza_daily/Klodzko_'+date_str+'.tif')

	#standaryzacja
	arr_GEM = (arr_GEM - df.model_value.mean())/df.model_value.std()
	arr_Pasquill = (arr_Pasquill - df.Pasquill_value.mean())/df.Pasquill_value.std()

	aa = rf.predict(list(zip(arr_GEM.flatten(),arr_Pasquill.flatten())))
	#aa = rf.predict(list(zip(arr_Pasquill.flatten(),arr_GEM.flatten())))
	arr_prediction = aa.reshape(arr_Pasquill.shape)
	arr_prediction = arr_prediction * df.monitoring_value.std() + df.monitoring_value.mean()
	CreateGeoTiff(output_folder+'Klodzko_'+date_str ,arr_prediction,geoTrans)

	print(start_date)
	start_date += delta