#!/usr/bin/env python
from data_loaders import *
from other_tools import *
import pprint as pp
import matplotlib.pyplot as plt
import pandas as pd
from config import *
from config import config as cfg 
import sys
import datetime as dt
import argparse

'''
Skrypt do wyciągania serii czasowej w danym punkcie z katalogu z godzinowymi tiffami
Wersja z więcej niż 1 stacją na obszarze obliczeń.
Skrypt łączy wyniki
- pomiarów, w kolumnach _measurement
- gaussa, w kolumnach _gauss
- GEMAQ, w kolumnach _ocena
- RF, w kolumnach _RF 
'''

parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('scenario', metavar='scenario', type=str,help="nazwa scenariusza")
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
args = parser.parse_args()
scenario = args.scenario

start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))

sufix='_meteo2_dow'
input_folder = 'tif_output/rf_model_'+sufix+'/'
#input_folder = 'tif_output/RF_hourly'+sufix+'/'

def extract_stacja(x_UTM,y_UTM,start_date ,end_date):
    ts=[]
    czas=[]
    delta = dt.timedelta(hours=1)
    start_date += delta
    while start_date <= end_date:
        date = start_date.strftime("%Y%m%d%H%M")
        rok = start_date.year
        miesiac = start_date.month
        day = start_date.day
        godzina = start_date.hour

    
        try:
            
            filename = input_folder+scenario+"_"+date+'.tif'
            arr,geoTrans = load_raster(filename)
            
            x,y = world2Pixel(geoTrans,x_UTM,y_UTM)
            c = arr[y,x]
            ts.append(c)
            try:
                czas_pomiaru = dt.datetime(rok,miesiac,day,godzina,0)
            except ValueError:
                czas_pomiaru = dt.datetime(rok,miesiac,day,0,0)
                czas_pomiaru += dt.timedelta(days=1)
                
            czas.append(czas_pomiaru)
        except Exception as e:
            print('error processing '+filename)
            print(e)
            pass
    
        start_date += delta
        #print(start_date.strftime("%Y%m%d"))

    df1 = pd.DataFrame(data=ts, index=czas)
    return df1


stacje = {'Klodzko':'DsKlodzSzkol','Bielski':'SlBielKossak',\
    'Bieszczadzki':'PkPolanZdrojMOB','Nowosadecki':'MpNoSaczNadb',\
    'Nowotarski':'MpNoTargPSlo','Zgorzelecki':'DsLubanMieszMOB',
    'Cieszynski':'SlCiesChopin','Zywiecki':'SlZywieKoper',\
    'Myslenicki':'MpMyslenSoli','Tatrzanski':'MpZakopaSien',\
    'Gorlicki':'MpSzymbaGorl','Krosnienski':'PkKrosKletow',\
    'Wadowicki':'MpSuchaNiesz',\
    'Walbrzyski':'DsWalbrzWyso','Kamiennogorski':'DsKamGoraMOB',\
    'Karkonoski':'DsJelGorOgin','Zabkowicki':'DsZabkPowWar',\
    'Nyski':'OpNysaRodzie','Glubczycki':'OpGlubRatusz',
    'Poludniowoslaskie':['SlBielKossak','SlCiesChopin','SlCiesMickie','SlUstronSana',\
        'SlZywieKoper','SlWodzGalczy','SlRybniBorki','SlCzerKopaln',\
        'MpSuchaNiesz','SlBielPartyz','SlZorySikor2','MpOswiecBema',\
        'SlGoczaUzdroMOB'],
    'Krakowskie':['MpKrakAlKras','MpKrakBulwar','MpKrakOsPias','MpKrakWadow',\
    'MpKrakZloRog','MpKrakDietla','MpKrakBujaka','MpKaszowLisz','MpZabieWapie','MpNiepo3Maja']
}


df_stacje = pd.read_csv('csv_input/wszystkie_stacje.csv',sep=';')
df_stacje = df_stacje.set_index(['station_code'])

wybrane_stacje =pd.DataFrame(df_stacje.loc[stacje[scenario]][['station_lat','station_long']])
wybrane_stacje['x_UTM']=0
wybrane_stacje['y_UTM']=0

for stacja in wybrane_stacje.iterrows():
    st_code = stacja[0]
    wybrane_stacje.loc[st_code,['x_UTM','y_UTM']]=lonlat2UTM(stacja[1]['station_long'],stacja[1]['station_lat'])


list_of_series=[]
list_of_cols=[]
for stacja in wybrane_stacje.iterrows():
    #print(stacja[1]['x_UTM'])
    #seria czasowa dla danej stacji
    df1 = extract_stacja(stacja[1]['x_UTM'],stacja[1]['y_UTM'],start_date,end_date)
    list_of_series.append(df1)
    list_of_cols.append(stacja[0])
    print(stacja[0])

res = pd.concat(list_of_series, axis=1)
res.columns = list_of_cols

#Pomiary GIOŚ
df_pomiary = pd.read_csv('csv_input/pomiaryPM10_2021.csv')
df_pomiary.set_index(pd.to_datetime(df_pomiary["to_timestamp"]),inplace=True)
del df_pomiary["to_timestamp"]
maska_pom = (df_pomiary['station_code'].isin(list_of_cols))
#zamiana kolumno-wartości na kolumny
df_pomiary = pd.pivot_table(df_pomiary[maska_pom],values='pm10',columns='station_code',index=df_pomiary[maska_pom].index)

#wyniki z Oceny dla stacji
df_ocena = pd.read_csv('csv_input/px_ocena_ts20210101.csv')
df_ocena.set_index(pd.to_datetime(df_ocena["Unnamed: 0"]),inplace=True)
del df_ocena["Unnamed: 0"]

#tymczas
end_date2 = dt.datetime(2021,12,31,1,0)
#wyniki Pasquilla
df_paszkwil = pd.read_csv('csv_output/'+scenario+'_'+start_date.strftime("%Y%m%d")+'_'+end_date2.strftime("%Y%m%d")+'.csv')
df_paszkwil.set_index(pd.to_datetime(df_paszkwil["Unnamed: 0"]),inplace=True)
del df_paszkwil["Unnamed: 0"]

#właściwe połączenie
res = res.join(df_pomiary,rsuffix='_measurement',lsuffix='_RF')
res = res.join(df_paszkwil.add_suffix('_gauss'))
res = res.join(df_ocena[list_of_cols].add_suffix('_ocena')) 
res.to_csv('csv_output/'+scenario+'_'+start_date.strftime('%Y%m%d')+'_'+end_date.strftime('%Y%m%d')+'_caloscHourly.csv')

