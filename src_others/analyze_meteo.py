import argparse
import datetime as dt
import pandas as pd 
import matplotlib.pyplot as plt 
import seaborn as sns
from calculations import get_stability_class,get_stability_class_by_dTdZ
import numpy as np 

'''
Skrypt do wczytania meteorologii, rysuje serie czasowe, histogramy itp.

'''
parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('scenario', metavar='scenario', type=str,help="nazwa scenariusza")
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
args = parser.parse_args()

start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))

scenario = args.scenario
input_file = 'csv_output/'+scenario+'_'+start_date.strftime("%Y%m%d")+'_'+end_date.strftime("%Y%m%d")+'_meteo.csv'

df = pd.read_csv(input_file)
df.set_index(pd.to_datetime(df['Unnamed: 0']),inplace=True)
del df['Unnamed: 0']

stab_lvl='28'
df['stab_class'+stab_lvl]=df["Ri"+stab_lvl].apply(get_stability_class)
df['stab_class'+stab_lvl]=df['stab_class'+stab_lvl].str[0]
df['stab_class_dTdz']=df["dTdz28"].apply(get_stability_class_by_dTdZ)
df['stab_class_dTdz']=df['stab_class_dTdz'].str[0]
df['month']=df.index.month

df.replace([np.inf],9999,inplace=True)

#order = ["('A', 0.08)","('B', 0.143)","('C', 0.196)","('D', 0.27)","('E', 0.363)","(F, 0.44)"]
order=["A","B","C","D","E","F"]
sns.set_theme(style="darkgrid")


ax= df["stab_class28"].sort_values().str[0].hist()
plt.title('HISTOGRAM_stab_class_by_Ri28_'+str(start_date.year))
plt.savefig('png/meteo/HISTOGRAM_Ri28_'+str(start_date.year))
plt.close()

df['stab_class_dTdz'].sort_values().str[0].hist()
plt.title('HISTOGRAM_stab_class_by_dTdz_'+str(start_date.year))
plt.savefig('png/meteo/HISTOGRAM_dTdz_'+str(start_date.year))
plt.close()

#stab class by dTdz
fig = sns.displot(df,x='vm',col="stab_class_dTdz",height=3,hue_order=order,kind="kde")
fig.fig.subplots_adjust(top=.8)
fig.fig.suptitle('VM by stab_class in '+scenario+' all year '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_vm_stab_dTdz_yearly.png')
plt.close()

fig=sns.displot(df,x='vd',col="stab_class_dTdz",height=3,hue_order=order,kind="kde")
fig.fig.subplots_adjust(top=.8)
fig.fig.suptitle('VD by stab_class in '+scenario+' all year '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_vd_stab_dTdz_yearly.png')
plt.close()

fig=sns.displot(df,x='Ri'+stab_lvl,col="stab_class_dTdz",height=3,hue_order=order,kind="kde")
fig.fig.subplots_adjust(top=.8)
fig.fig.suptitle('Ri by stab_class in '+scenario+' all year '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_Ri_stab_dTdz_yearly.png')
plt.close()

fig=sns.displot(df,x='temp'+stab_lvl,col="stab_class_dTdz",height=3,hue_order=order,kind="kde")
fig.fig.subplots_adjust(top=.8)
fig.fig.suptitle('Temperature by stab_class in '+scenario+' all year '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_temp_stab_dTdz_yearly.png')
plt.close()

fig=sns.displot(df,x='dTdz28',col="stab_class_dTdz",height=3,hue_order=order,kind="kde")
fig.fig.subplots_adjust(top=.8)
fig.fig.suptitle('dTdz by stab_class in '+scenario+' all year '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_dTdz_stab_dTdz_yearly.png')
plt.close()


df["rownowaga_Ri28"] = ""
maska = ((df["stab_class28"] == "F")| (df["stab_class28"] == "E"))
df["rownowaga_Ri28"].where(~maska, "STAŁA",inplace=True)
maska = ((df["stab_class28"] == "A")| (df["stab_class28"] == "B") |(df["stab_class28"] == "C"))
df["rownowaga_Ri28"].where(~maska, "CHWIEJNA",inplace=True)
maska =  (df["stab_class28"] == "D")
df["rownowaga_Ri28"].where(~maska, "OBOJĘTNA",inplace=True)

df["rownowaga_dTdz"] = ""
maska = ((df["stab_class_dTdz"] == "F")| (df["stab_class_dTdz"] == "E"))
df["rownowaga_dTdz"].where(~maska, "STAŁA",inplace=True)
maska = ((df["stab_class_dTdz"] == "A")| (df["stab_class_dTdz"] == "B") |(df["stab_class_dTdz"] == "C"))
df["rownowaga_dTdz"].where(~maska, "CHWIEJNA",inplace=True)
maska =  (df["stab_class_dTdz"] == "D")
df["rownowaga_dTdz"].where(~maska, "OBOJĘTNA",inplace=True)

stala=[]
chwiejna=[]
obojetna=[]

for month in range(1,13):
    df2 = df[df.month == month]

    stala.append(df2["rownowaga_dTdz"].value_counts(normalize=True)['STAŁA'])
    try:
        chwiejna.append(df2["rownowaga_dTdz"].value_counts(normalize=True)['CHWIEJNA'])
    except KeyError:
        chwiejna.append(0)
    try:
        obojetna.append(df2["rownowaga_dTdz"].value_counts(normalize=True)['OBOJĘTNA'])
    except KeyError:
        obojetna.append(0)
    
plt.figure(figsize=(12,8))
plt.plot(np.arange(1,13),np.array(stala)*100)
plt.plot(np.arange(1,13),np.array(chwiejna)*100)
plt.plot(np.arange(1,13),np.array(obojetna)*100)
plt.xlabel('miesiąc w roku '+str(start_date.year))
plt.ylabel('[%]')
plt.ylim([0 ,100])
plt.legend(['STAŁA','CHWIEJNA','OBOJĘTNA'])
plt.title('Stany równowagi wg. gradientowej liczby Richardsona z modelu GEM-AQ w roku '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_Rownowaga_dTdz_'+str(start_date.year)+'.png')
plt.close()

stala=[]
chwiejna=[]
obojetna=[]

for month in range(1,13):
    df2 = df[df.month == month]

    stala.append(df2["rownowaga_Ri28"].value_counts(normalize=True)['STAŁA'])
    try:
        chwiejna.append(df2["rownowaga_Ri28"].value_counts(normalize=True)['CHWIEJNA'])
    except KeyError:
        chwiejna.append(0)
    try:
        obojetna.append(df2["rownowaga_Ri28"].value_counts(normalize=True)['OBOJĘTNA'])
    except KeyError:
        obojetna.append(0)
    
plt.figure(figsize=(12,8))
plt.plot(np.arange(1,13),np.array(stala)*100)
plt.plot(np.arange(1,13),np.array(chwiejna)*100)
plt.plot(np.arange(1,13),np.array(obojetna)*100)
plt.xlabel('miesiąc w roku '+str(start_date.year))
plt.ylabel('[%]')
plt.ylim([0 ,100])
plt.legend(['STAŁA','CHWIEJNA','OBOJĘTNA'])
plt.title('Stany równowagi wg. gradientu Temperatury z modelu GEM-AQ w roku '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_Rownowaga_Ri_'+str(start_date.year)+'.png')
plt.close()

'''
# Stabclass wg. Ri
fig = sns.displot(df,x='vm',col="stab_class"+stab_lvl,height=3,hue_order=order,kind="kde")
fig.fig.subplots_adjust(top=.8)
fig.fig.suptitle('VM by stab_class in '+scenario+' all year '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_vm_stab'+stab_lvl+'_yearly.png')
plt.close()

fig=sns.displot(df,x='vd',col="stab_class"+stab_lvl,height=3,hue_order=order,kind="kde")
fig.fig.subplots_adjust(top=.8)
fig.fig.suptitle('VD by stab_class in '+scenario+' all year '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_vd_stab'+stab_lvl+'_yearly.png')
plt.close()

fig=sns.displot(df,x='Ri'+stab_lvl,col="stab_class"+stab_lvl,height=3,hue_order=order,kind="kde")
fig.fig.subplots_adjust(top=.8)
fig.fig.suptitle('Ri by stab_class in '+scenario+' all year '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_Ri_stab'+stab_lvl+'_yearly.png')
plt.close()

fig=sns.displot(df,x='temp',col="stab_class"+stab_lvl,height=3,hue_order=order,kind="kde")
fig.fig.subplots_adjust(top=.8)
fig.fig.suptitle('Temperature by stab_class'+stab_lvl+' in '+scenario+' all year '+str(start_date.year))
plt.savefig('png/meteo/'+scenario+'_temp_stab'+stab_lvl+'_yearly.png')
plt.close()


for month in range(1,13):
    fig = sns.displot(df[df.month==month],x='vm',col="stab_class"+stab_lvl,height=3,hue_order=order,kind="kde")
    fig.fig.subplots_adjust(top=.8)
    fig.fig.suptitle('VM by stab_class in '+scenario+' '+str(start_date.year)+str(month).zfill(2))
    plt.savefig('png/meteo/'+scenario+'_vm_stab'+stab_lvl+'_month_'+str(month).zfill(2)+'.png')
    plt.close()

    fig = sns.displot(df[df.month==month],x='vd',col="stab_class"+stab_lvl,height=3,hue_order=order,kind="kde")
    fig.fig.subplots_adjust(top=.8)
    fig.fig.suptitle('VD by stab_class in '+scenario+' '+str(start_date.year)+str(month).zfill(2))
    plt.savefig('png/meteo/'+scenario+'_vd_stab'+stab_lvl+'_month_'+str(month).zfill(2)+'.png')
    plt.close()


    fig = sns.displot(df[df.month==month],x='temp',col="stab_class"+stab_lvl,height=3,hue_order=order,kind="kde")
    fig.fig.subplots_adjust(top=.8)
    fig.fig.suptitle('Temp by stab_class in '+scenario+' '+str(start_date.year)+str(month).zfill(2))
    plt.savefig('png/meteo/'+scenario+'_vd_stab'+stab_lvl+'_month_'+str(month).zfill(2)+'.png')
    plt.close()


    fig = sns.displot(df[df.month==month],x='vd',col="stab_class"+stab_lvl,height=3,hue_order=order,kind="kde")
    fig.fig.subplots_adjust(top=.8)
    fig.fig.suptitle('Ri'+stab_lvl+' by stab_class in '+scenario+' '+str(start_date.year)+str(month).zfill(2))
    plt.savefig('png/meteo/'+scenario+'_Ri_stab'+stab_lvl+'_month_'+str(month).zfill(2)+'.png')
    plt.close()
'''