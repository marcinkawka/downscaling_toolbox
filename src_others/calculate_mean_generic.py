#!/usr/bin/env python

from data_loaders import *
import matplotlib.pyplot as plt
import datetime as dt
import argparse
import glob
import os

parser = argparse.ArgumentParser(description='podaj nazwę scenariusza')
parser.add_argument('scenario', metavar='scenario', type=str,help="nazwa scenariusza")
args = parser.parse_args()
scenario = args.scenario

if scenario in ['Klodzko','Zgorzelecki','Karkonoski','Zabkowicki',\
	'Walbrzyski','Glubczycki','Nyski','Kamiennogorski']:
	UTM=33
else:
	UTM=34

sufix='_dow_meteo2'
sufix='_dow'
sufix='_meteo2'
sufix='_meteo2_dow'
#input_folder = 'tif_output/LSTM_hourly/'
#input_folder = 'tif_output/rf_model_'+sufix+'/'
input_folder = 'tif_output/rgb_model_'+sufix+'/'
#input_folder = 'tif_output/hourly/'+scenario+'/'
output_folder='tif_final/'



res = []
i=0
print(input_folder)
for file in glob.glob(input_folder+"/"+scenario+"*.tif"):
    print(file)
    arr,geoTrans = load_raster(file)

    res.append(arr)
    i+=1
    #if i>5:
    #    break

print(str(i)+' files loaded')

all_res = np.array(res)

print('Calculating statistics... this may take a while')
res_mean = all_res.mean(axis=0)

#CreateGeoTiff(output_folder+scenario+'_mean_RF'+sufix ,res_mean,geoTrans,UTM=UTM)
CreateGeoTiff(output_folder+scenario+'_mean_rgb'+sufix ,res_mean,geoTrans,UTM=UTM)
