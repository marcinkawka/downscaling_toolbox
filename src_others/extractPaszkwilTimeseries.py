#!/usr/bin/env python
from data_loaders import *
from other_tools import *
import pprint as pp
import matplotlib.pyplot as plt
import pandas as pd
from config import *
from config import config as cfg 
import sys
import datetime as dt
import argparse

'''
Skrypt do wyciągania serii czasowej w danym punkcie z katalogu z godzinowymi tiffami
'''

parser = argparse.ArgumentParser(description='podaj początkową i końcową datę')
parser.add_argument('scenario', metavar='scenario', type=str,help="nazwa scenariusza")
parser.add_argument('d0', metavar='start_date', type=str,help="data początkowaYYYYDDMM")
parser.add_argument('dk', metavar='end_date', type=str,help="data końcowa YYYYDDMM")
args = parser.parse_args()
scenario = args.scenario

start_date = dt.datetime(int(args.d0[:4]),int(args.d0[4:6]),int(args.d0[6:]))
end_date = dt.datetime(int(args.dk[:4]),int(args.dk[4:6]),int(args.dk[6:]))


stacje_lon={"Klodzko":16.65365,"Bielski":19.027318,'Zgorzelecki':15.2860,\
    'Nowosadecki':20.7142,'Bieszczadzki':22.439,'Nowotarski':20.02926}
stacje_lat={"Klodzko":50.43348,"Bielski":49.813464,'Zgorzelecki':51.1186,\
    'Nowosadecki':49.6194,'Bieszczadzki':49.377,'Nowotarski':49.48372}

stacje = {'Klodzko':'DsKlodzSzkol','Bielski':'SlBielKossak',\
    'Bieszczadzki':'PkPolanZdrojMOB','Nowosadecki':'MpNoSaczNadb',\
    'Nowotarski':'MpNoTargPSlo','Zgorzelecki':'DsLubanMieszMOB',
    'Cieszynski':'SlCiesChopin','Zywiecki':'SlZywieKoper',\
    'Myslenicki':'MpMyslenSoli','Tatrzanski':'MpZakopaSien',\
    'Gorlicki':'MpSzymbaGorl','Krosnienski':'PkKrosKletow',\
    'Wadowicki':'MpSuchaNiesz',\
    'Walbrzyski':'DsWalbrzWyso','Kamiennogorski':'DsKamGoraMOB',\
    'Karkonoski':'DsJelGorOgin','Zabkowicki':'DsZabkPowWar',\
    'Nyski':'OpNysaRodzie','Glubczycki':'OpGlubRatusz'
}

df_stacje = pd.read_csv('csv_input/wszystkie_stacje.csv',sep=';')
df_stacje = df_stacje.set_index(['station_code'])
lat_stacji =df_stacje.loc[stacje[scenario]]['station_lat']
lon_stacji =df_stacje.loc[stacje[scenario]]['station_long']

x_UTM,y_UTM =lonlat2UTM(lon_stacji,lat_stacji)


ts=[]
czas=[]
delta = dt.timedelta(days=1)

while start_date <= end_date:
    date = start_date.strftime("%Y%m%d")
    rok = start_date.year
    miesiac = start_date.month
    day = start_date.day

    for godzina in range(1,25):
        try:
            
            filename = cfg['OUTPUT']['tif_hourly_dir']+scenario+"/"+scenario+"_"+date+'_'+str(godzina)+'.tif'
            arr,geoTrans = load_raster(filename)
            
            x,y = world2Pixel(geoTrans,x_UTM,y_UTM)
            c = arr[y,x]
            ts.append(c)
            try:
                czas_pomiaru = dt.datetime(rok,miesiac,day,godzina,0)
            except ValueError:
                czas_pomiaru = dt.datetime(rok,miesiac,day,0,0)
                czas_pomiaru += dt.timedelta(days=1)
                
            czas.append(czas_pomiaru)
        except Exception as e:
            print('error processing '+filename)
            print(e)
            pass
    
    start_date += delta
    #print(start_date.strftime("%Y%m%d"))

df1 = pd.DataFrame(data=ts, index=czas)
df1.to_csv('csv_output/'+scenario+'_'+str(args.d0)+'.csv')
#plt.plot(czas,ts)
#plt.show()
